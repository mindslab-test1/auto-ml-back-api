package ai.mindslab.automl.backend.batch;

import ai.mindslab.automl.backend.model.entity.project.Queue;
import ai.mindslab.automl.backend.service.GraphDataService;
import ai.mindslab.automl.backend.service.ModelTestService;
import ai.mindslab.automl.backend.service.QueueService;
import ai.mindslab.automl.backend.service.TrainingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
public class BatchController {
    private static final Logger logger = LoggerFactory.getLogger(BatchController.class.getName());

    private final TrainingService trainingService;
    private final QueueService queueService;
    private final GraphDataService graphDataService;
    private final ModelTestService modelTestService;

    /**
     * batchQueue
     * 학습이나 테스트에 사용되는 resource (GPU)들을 조회하여 현재 사용중이지 않은 resource를 찾는다.
     * 사용 가능한 resource의 갯수만큼 대기열 (queue)에서 가장 먼저 들어온 순서부터 학습 생성으로 전송되며,
     * 대기열에서는 삭제해준다. 학습 생성 후 오류가 발생했을 시에도 대기열에서는 제거된다.
     */
    //    @Scheduled(cron = "0 0/1 * * * ?")
    @Scheduled(cron = "*/20 * * * * ?")
    public void batchQueue() {
        logger.info("[Batch Train Queue] Begin!");
        int resourceSize = trainingService.getAvailableResourcesSize();
        if (resourceSize > 0) {
            List<Queue> queues = queueService.getQueueListOrderByRegDateAsc();
            if (queues.size() > 0) {
                logger.info("[Batch Train Queue] Moving queued models to training!");
                for (int i = 0; i < resourceSize && i < queues.size(); i++) {
                    try {
                        Queue currQueue = queues.get(i);
                        queueService.deleteQueue(currQueue.getSnapshotId());
                        if (currQueue.getQueueType().equals("training")) {
                            trainingService.createTraining(currQueue.getSnapshotId());
                        }
                        else if (currQueue.getQueueType().equals("testing")) {
                            modelTestService.createModelTest(currQueue.getSnapshotId());
                        }
                        logger.info("[Batch Train Queue] Finished Successfully!");
                    } catch (Exception e) {
                        logger.error("[Batch Train Queue] Error occurred while moving queued models to training {}", queues.get(i));
                    }
                }

            }
        }
    }

    // Todo: Add Scheduler Controller to periodically request log data from TEN AI's 전체 학습 조회

    /**
     * checkAutoFinished
     * 학습 서버쪽에서 목표 epoch 달성으로 자동 완료된 학습들을 조회하여, DB에서 관리되는 ModelInTraining에
     * 저장되어 있는 학습들과 비교하여 다음 조건들을 확인한다.
     * 1. 자동 완료된 학습이 ModelInTraining에 아직 기록 되어 있는지
     * 2. 위의 학습의 그래프 정보가 목표까지 업데이트 되었는지
     * 위 두 조건들이 충족된 학습은 ModelInTraining에서 지워준다.
     */
//    @Scheduled(cron = "0 0/1 * * * ?")
    @Scheduled(cron = "*/120 * * * * ?")
    public void batchCheckStatus() {
        logger.info("[Batch Check Status] Begin!");
        trainingService.checkRunningStatus();
        logger.info("[Batch Check Status] Finished Successfully!");
    }

    // Todo: Add Scheduler Controller to periodically request log data from TEN AI's 학습 로그 조회

    /**
     * getTrainingLog
     * ModelInTraining에 기록되어있는 진행중인 학습들을 리스트로 불러와,
     * 각각 학습에 대한 새로운 epoch log 정보를 불러온다.
     * log를 Graph Data로 변경하여 저장해준다.
     */
//    @Scheduled(cron = "0 0/1 * * * ?")
    @Scheduled(cron = "*/30 * * * * ?")

    public void batchUpdateGraphData() {
        logger.info("[Batch Update Graph Data] Begin!");
        graphDataService.updateGraphData();
        logger.info("[Batch Update Graph Data] Finished Successfully!");
    }
}
