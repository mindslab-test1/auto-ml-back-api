package ai.mindslab.automl.backend.service;

import ai.mindslab.automl.backend.model.entity.project.ModelInTraining;
import ai.mindslab.automl.backend.repository.ModelInTrainingRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class ModelInTrainingService {
    private static final Logger logger = LoggerFactory.getLogger(ModelInTrainingService.class.getName());

    private final ModelInTrainingRepository modelInTrainingRepository;

    /**
     * addModelForTraining
     * 학습 서버에 생성된 학습 세션을 ModelInTraining 테이블에 기록하는 기능
     * ModelInTraining에 기록된 학습은 진행중임을 나타내며, 학습 완료되면 다른 기능에 의해 삭제됨
     * @param projectSnapshotId 학습과 연관된 projectSnapshot에 대한 id
     * @param maxEpoch 학습의 목표 epoch
     * @param regDate 학습 생성 일자
     * @param modelName 화면설계서상 학습 모델 아이디 "mdl_userID_엔진타입_생성일시"
     */
    public void addModelForTraining(Long projectSnapshotId, int maxEpoch, String regDate, String modelName) {
        logger.info("Register new model-in-training to ModelInTraining table, projectSnapshotId: {}; trainingId: {}",
                projectSnapshotId, modelName);
        ModelInTraining entity = new ModelInTraining();
        entity.setProjectSnapshotId(projectSnapshotId);
        entity.setMaxEpoch(maxEpoch);
        entity.setCurrEpoch(0);
        entity.setRegDt(LocalDateTime.parse(regDate));
        entity.setTrainingId(modelName);
        modelInTrainingRepository.save(entity);
    }

    /**
     * deleteBySnapshotId
     * ModelInTraining 테이블에서 학습을 projectSnapshotId로 조회하여 삭제하는 기능
     * @param projectSnapshotId 학습과 연관된 projectSnapshot에 대한 id
     */
    public void deleteBySnapshotId (Long projectSnapshotId) {
        logger.info("Deleting a model-in-training by projectSnapshotId: {}...", projectSnapshotId);
        Optional<ModelInTraining> optional = modelInTrainingRepository.findByProjectSnapshotId(projectSnapshotId);
        if (optional.isPresent()) {
            modelInTrainingRepository.delete(optional.get());
        }
        else {
            throw new EmptyResultDataAccessException(0);
        }
    }

    /**
     * getCurrEpochByProjectSnapshotId
     * ModelInTraining에서 projectSnapshotId로 조회된 entity에서 currEpoch를 출력해주는 기능
     * currEpoch = 몇 epoch까지 그래프가 업데이트 되었는지를 알려주는 column
     * @param projectSnapshotId 학습과 연관된 projectSnapshot에 대한 id
     * @return int most-recent updated epoch
     */
    public int getCurrEpochByProjectSnapshotId (Long projectSnapshotId) {
        logger.info("Getting the most recently updated epoch for projectSnapshotId: {}...", projectSnapshotId);
        Optional<ModelInTraining> optional = modelInTrainingRepository.findByProjectSnapshotId(projectSnapshotId);
        if (optional.isPresent()) {
            return optional.get().getCurrEpoch();
        }
        else {
            throw new EmptyResultDataAccessException(0);
        }
    }

    /**
     * getAll
     * ModelInTraining에 기록된 학습들을 전부 가져와 list로 출력해주는 기능
     * @return List<ModelInTraining> 진행중인 학습 리스트
     */
    public List<ModelInTraining> getAll () {
        logger.info("Getting all models in training...");
        Iterable<ModelInTraining> iterable = modelInTrainingRepository.findAll();
        List<ModelInTraining> list = new ArrayList<>();
        iterable.forEach(list::add);
        return list;
    }

    /**
     * getBytrainingId
     * ModelInTraining에 기록된 진행중이 학습을 학습 모델 아이디로 조회하는 기능
     * @param trainingId 화면설계서상 학습 모델 아이디 "mdl_userID_엔진타입_생성일시"
     * @return ModelInTraining 모델 아이디로 조회된 학습 entity
     */
    public ModelInTraining getByTrainingId(String trainingId) {
        logger.info("Retrieving model-in-training by trainingId: {}...", trainingId);
        return modelInTrainingRepository.findByTrainingId(trainingId).orElse(null);
    }
}
