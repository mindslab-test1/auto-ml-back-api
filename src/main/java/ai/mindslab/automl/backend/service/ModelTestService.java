package ai.mindslab.automl.backend.service;

import ai.mindslab.automl.backend.client.TenAiRestClient;
import ai.mindslab.automl.backend.client.exception.RestTemplateException;
import ai.mindslab.automl.backend.client.model.request.EnvReq;
import ai.mindslab.automl.backend.client.model.request.TestForm;
import ai.mindslab.automl.backend.client.model.request.VolReq;
import ai.mindslab.automl.backend.client.model.response.tenai.StateMessage;
import ai.mindslab.automl.backend.client.model.response.tenai.TestedModel;
import ai.mindslab.automl.backend.model.dto.test.ModelTestDto;
import ai.mindslab.automl.backend.model.dto.training.ProjectSnapshotDto;
import ai.mindslab.automl.backend.model.entity.project.ModelTestSnapshot;
import ai.mindslab.automl.backend.model.entity.project.Queue;
import ai.mindslab.automl.backend.payload.Response;
import ai.mindslab.automl.backend.payload.TestModelResult;
import ai.mindslab.automl.backend.repository.ModelTestSnapshotRepository;
import ai.mindslab.automl.backend.repository.ProjectSnapshotRepository;
import ai.mindslab.automl.backend.service.util.FfmpegService;
import ai.mindslab.automl.backend.service.util.ModelMapperService;
import ai.mindslab.automl.backend.service.util.ParamConverterService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class ModelTestService {
    private static final Logger logger = LoggerFactory.getLogger(ModelTestService.class.getName());

    private final TenAiRestClient restClient;

    private final ModelTestSnapshotRepository modelTestSnapshotRepository;
    private final ProjectSnapshotRepository projectSnapshotRepository;

    private final FfmpegService ffmpegService;
    private final ModelMapperService modelMapperService;
    private final TrainingService trainingService;
    private final QueueService queueService;
    private final ParamConverterService paramConverterService;

    @Value("${docker.image.test}")
    private String dockerImageTest;

    @Value("${api.tenai.url}")
    private String tenaiUrl;

    public String addTimestampToFileName(String fileName) throws IllegalArgumentException {
        String fileNameMinusExt = fileName.split("\\.")[0];
        return fileNameMinusExt + "_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")) + ".wav";
    }

    public ModelTestSnapshot saveModelTestSnapshot(ModelTestDto dto) {
        logger.info("Saving modelTestDto into modelTestSnapshot");
        ModelTestSnapshot snapshot = new ModelTestSnapshot();
        snapshot.setAmFileName(dto.getAmFileName());
        snapshot.setAmFilePath(dto.getAmFilePath());
        snapshot.setModelId(dto.getModelId());
        snapshot.setDataFileName(dto.getDataFileName());
        snapshot.setDataFilePath(dto.getDataFilePath());
        snapshot = modelTestSnapshotRepository.save(snapshot);

        snapshot.setModelTestId(String.format("%s_%s_%s",
                snapshot.getId(),
                dto.getAmFileName().replace(".", "__"),
                LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"))));
        return modelTestSnapshotRepository.save(snapshot);
    }

    public ResponseEntity<Response> sendToResourceOrQueue(Long modelTestSnapshotId) {
        if (trainingService.checkAvailableResources() && !queueService.checkQueueExists()) {
            logger.info("Adding model for testing, modelTestSnapshotId: {}", modelTestSnapshotId);
            createModelTest(modelTestSnapshotId);
            return new ResponseEntity<>(new TestModelResult(true, "", "Running", "", modelTestSnapshotId, null), HttpStatus.OK);
        } else {
            logger.info("Resource not available! Adding model to queue, modelTestSnapshotId: {}", modelTestSnapshotId);
            queueService.addQueue(modelTestSnapshotId, "testing");
            return new ResponseEntity<>(new TestModelResult(true, "", "Waiting", "", modelTestSnapshotId, null), HttpStatus.OK);
        }
    }

    /**
     * setModelTestFormParams
     * 학습된 모델을 테스트 서버에 모델 테스트 생성 요청하기 위해 POST request 요청 파라미터를 만들어줌.
     *
     * @param dto      테스트 생성에 필요한 파라미터 정보가 들어간 object. am 학습모델의 경로와 이름, 음성파일의 경로와 이름,
     *                 그리고 lm 모델의 언어, 샘플링레이트, eos 사용 여부가 들어가있음.
     * @param envReqs  테스트 생성에 필요한 환경 변수 파라미터 목록. 처음엔 비어있음.
     * @param volReqs  테스트 생성시 생성되는 컨테이너 환경에 첨부될 볼륨 정보 목록. 처음엔 비어있음.
     * @param commands 테스트 생성 요청시 명시해야 하는 명령어 목록. 처음엔 비어있음.
     */
    private void setModelTestFormParams(ModelTestDto dto, List<EnvReq> envReqs, List<VolReq> volReqs, List<String> commands) {
        logger.info("Configuring the parameters for model testing session request form...");

        commands.add("--vadremote=" + tenaiUrl + ":32001");
        commands.add("--nthread_decoder=75");
        commands.add("--usedetailsegment=false");
        commands.add("--am=/AM/" + dto.getAmFileName());
        commands.add("--data=/DATA/" + dto.getDataFileName());

        envReqs.add(new EnvReq("LC_ALL", "C.UTF-8"));
        
        Long projectSnapshotId = Long.parseLong(dto.getModelId().split("_")[3]);
        String language = projectSnapshotRepository.findLanguageByProjectSnapshotId(projectSnapshotId);
        language = paramConverterService.convertLangCode(language);
        if (language.equals("eng")) {
            volReqs.add(new VolReq("/AUTOML/models/common/base_eng_16k", "/MODEL"));
        }
        else {
            volReqs.add(new VolReq("/AUTOML/models/common/base_kor_8k_eos", "/MODEL"));
        }
        volReqs.add(new VolReq(dto.getAmFilePath(), "/AM"));
        volReqs.add(new VolReq(dto.getDataFilePath(), "/DATA"));
        logger.info("Model test request parameters configured successfully!");
    }

    /**
     * createModelTest
     * ModelTestSnapshot에 저장된 테스트 생성 파라미터를 불러와 테스트 생성 요청을 만든 후, 요청을 테스트 서버에 보내준다.
     *
     * @param modelTestSnapshotId 테스트 생성 요청을 위한 파라미터 정보가 담긴 ModelTestSnapshot 객체의 ID.
     */
    public void createModelTest(Long modelTestSnapshotId) throws RuntimeException {
        logger.info("Creating new model test session, modelTestSnapshotId: {}...", modelTestSnapshotId);

        ModelTestSnapshot snapshot = modelTestSnapshotRepository.findById(modelTestSnapshotId).orElse(null);


        ModelTestDto dto = modelMapperService.map(snapshot, ModelTestDto.class);

        List<EnvReq> envReqs = new ArrayList<>();
        List<VolReq> volReqs = new ArrayList<>();
        List<String> commands = new ArrayList<>();
        setModelTestFormParams(dto, envReqs, volReqs, commands);


        TestForm modelTestForm = TestForm.builder().name(snapshot.getModelTestId())
                .image(dockerImageTest)
                .command(commands)
                .env(envReqs)
                .volumes(volReqs)
                .build();

        logger.info("Snapshot mapped to testForm {}", modelTestForm);
        try {
            logger.info("Sending the testForm to the model testing server...");
            restClient.addModelToTest(modelTestForm);
        } catch (Exception e) {
            logger.error("Error occurred while attempting to create a new model test session. ModelTestId: {}", snapshot.getModelTestId());
            throw RestTemplateException.builder().error(e.getMessage()).build();
        }
        logger.info("Model test successfully created. ModelTestId: {}", modelTestForm.getName());
    }

    public TestedModel getTestedModelResult(Long modelTestSnapshotId) {
        logger.info("Getting model test result, modelTestSnapshotId: {}...", modelTestSnapshotId);
        TestedModel testedModel = new TestedModel();

        Queue queue = queueService.getQueueBySnapshotId(modelTestSnapshotId);
        
        if (queue != null) {
            List<Queue> queueList = queueService.getQueueListOrderByRegDateAsc();
            Integer queueIndex = queueList.indexOf(queue);
            StateMessage stateMessage = new StateMessage();
            stateMessage.setType("Waiting");
            testedModel = TestedModel.builder()
                              .state(stateMessage)
                              .queueIndex(queueIndex)
                              .build();
        } else {
            ModelTestSnapshot snapshot = modelTestSnapshotRepository.findById(modelTestSnapshotId).orElse(null);
            if (snapshot != null) {
                try {
                    testedModel = restClient.getTestedModel(snapshot.getModelTestId());
                } catch (Exception e) {
                    logger.error("Error occurred while attempting to retrieve a running model test session. ModelTestId: {}", snapshot.getModelTestId());
                    throw RestTemplateException.builder().error(e.getMessage()).build();
                }
                if (testedModel.getResult() != null) {
                    modelTestSnapshotRepository.updateResultById(modelTestSnapshotId, testedModel.getResult());
                }
            }
        }

        return testedModel;
    }

    public void deleteModelsInTesting(Long modelTestSnapshotId) {
        logger.info("Deleting model test result, modelTestSnapshotId: {}...", modelTestSnapshotId);
        ModelTestSnapshot snapshot = modelTestSnapshotRepository.findById(modelTestSnapshotId).orElse(null);
        if (snapshot != null) {
            try {
                restClient.deleteModelsInTesting(snapshot.getModelTestId());
            } catch (Exception e) {
                logger.error("Model test not found on training server! ModelTestId: {}", snapshot.getModelTestId());
                throw RestTemplateException.builder().error(e.getMessage()).build();
            }
        }
    }

    public File convertTestDataSampleRate(MultipartFile dataFile, String modelId) throws IOException {

        String snapshot = projectSnapshotRepository.findSnapshotByProjectModelId(modelId);

        ObjectMapper objectMapper = new ObjectMapper();
        ProjectSnapshotDto snapshotDto = objectMapper.readValue(snapshot, ProjectSnapshotDto.class);

        return ffmpegService.convertSampleRate(dataFile, Integer.parseInt(paramConverterService.convertSampleRate(snapshotDto.getSampleRate())));
    }
}
