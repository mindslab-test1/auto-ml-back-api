package ai.mindslab.automl.backend.service.util;

import org.springframework.stereotype.Service;

@Service
public class ParamConverterService {
    public String convertLangCode(String lang) {
        switch (lang) {
            case "LN_KO":
                return "kor";
            case "LN_EN":
                return "eng";
            default:
                return "";
        }
    }

    public String convertSampleRate(String sampleRate) {
        switch (sampleRate) {
            case "SR001":
                return "8000";
            case "SR002":
                return "16000";
            default:
                return "";
        }
    }

    public String convertEos(String eos) {
        switch (eos) {
            case "Y":
                return "true";
            case "N":
                return "false";
            default:
                return "";
        }
    }

}
