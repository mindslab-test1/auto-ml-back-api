package ai.mindslab.automl.backend.service.util;

public class FfmpegUtils {
    public static String ffmpegConvertSampleRate(String inputFileName, String outputFileName, int sampleRate) {
        String ffmpegCommand = "ffmpeg -i " + inputFileName + " -ar " + sampleRate + " " + outputFileName;
        return ffmpegCommand;
    }
}
