package ai.mindslab.automl.backend.service;

import ai.mindslab.automl.backend.model.entity.project.Queue;
import ai.mindslab.automl.backend.repository.ProjectSnapshotRepository;
import ai.mindslab.automl.backend.repository.QueueRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class QueueService {
    private static final Logger logger = LoggerFactory.getLogger(QueueService.class.getName());

    private final QueueRepository queueRepository;
    private final ProjectSnapshotRepository projectSnapshotRepository;

    /**
     * getQueueListOrderByRegDateAsc
     * 대기열 (queue)에 기록된 대기중 학습들을 전부 불러와 먼저 등록된 순서대로 list를 출력해주는 기능
     * @return List<Queue> 날짜로 나열된 대기열 목록
     */
    public List<Queue> getQueueListOrderByRegDateAsc() {
        logger.info("Getting the queue list ordered by registered date ascending...");
        return queueRepository.findAllByOrderByRegDtAsc();
    }

    /**
     * addQueue
     * 대기열 (queue)에 새로운 대기중 학습을 추가해주는 기능
     * @param snapshotId 학습과 연관된 projectSnapshot에 대한 id
     */
    public void addQueue(Long snapshotId, String queueType) {
        logger.info("[addQueue] Adding new projectSnapshot to the queue list, projectSnapshotId: {}...", snapshotId);
        try {
            Queue queue = new Queue();
            queue.setSnapshotId(snapshotId);
            queue.setQueueType(queueType);
            queue.setRegDt(LocalDateTime.now());
            queueRepository.save(queue);
            projectSnapshotRepository.updateStatusByProjectSnapshotId("q", snapshotId);
        } catch (Exception e) {
            logger.error("[addQueue] Error s{}", e.getMessage());

        }

    }

    /**
     * checkQueueExists
     * 대기열 (queue)에 대기중 학습이 하나라도 존재하는지 확인해주는 기능
     * @return boolean 대기중 학습이 하나라도 존재한다면 true, 대기열이 비어있다면 false
     */
    public boolean checkQueueExists() {
        logger.info("Checking if the queue has any training requests...");
        return queueRepository.findAll().size() > 0;
    }

    /**
     * getQueueBySnapshotId
     * 대기열 (queue)에 있는 대기중 학습을 projectSnapshotId로 조회하는 기능
     * @param projectSnapshotId 학습과 연관된 projectSnapshot에 대한 id
     * @return Queue 조회된 대기중 학습
     */
    public Queue getQueueBySnapshotId(Long projectSnapshotId) {
        logger.info("Retrieving a training request in queue by projectSnapshotId: {}...", projectSnapshotId);
        return queueRepository.findBySnapshotId(projectSnapshotId).orElse(null);
    }

    /**
     * deleteQueue
     * 대기열 (queue)에 있는 대기중 학습을 projectSnapshotId로 조회한 후 대기열에서 제거해주는 기능
     * @param snapshotId 학습과 연관된 projectSnapshot에 대한 id
     */
    public void deleteQueue(Long snapshotId) {
        logger.info("Removing a training/testing request from the queue list, snapshotId: {}...", snapshotId);
        Queue queue = getQueueBySnapshotId(snapshotId);
        if (queue != null) {
            queueRepository.delete(queue);
        } else {
            logger.error("SnapshotId \"{}\" could not be found in queue!", snapshotId);
        }

    }
}
