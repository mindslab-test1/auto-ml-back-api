package ai.mindslab.automl.backend.service;

import ai.mindslab.automl.backend.client.DataRestClient;
import ai.mindslab.automl.backend.client.TenAiRestClient;
import ai.mindslab.automl.backend.client.exception.RestTemplateException;
import ai.mindslab.automl.backend.client.model.request.EnvReq;
import ai.mindslab.automl.backend.client.model.request.ModelForm;
import ai.mindslab.automl.backend.client.model.request.TrainForm;
import ai.mindslab.automl.backend.client.model.request.VolReq;
import ai.mindslab.automl.backend.client.model.response.tenai.Resource;
import ai.mindslab.automl.backend.client.model.response.tenai.StateMessage;
import ai.mindslab.automl.backend.client.model.response.tenai.TrainedModel;
import ai.mindslab.automl.backend.client.model.response.tenai.TrainedModelFile;
import ai.mindslab.automl.backend.model.dto.training.ProjectSnapshotDto;
import ai.mindslab.automl.backend.model.entity.project.GraphData;
import ai.mindslab.automl.backend.model.entity.project.ModelInTraining;
import ai.mindslab.automl.backend.model.entity.project.ProjectSnapshot;
import ai.mindslab.automl.backend.repository.GraphDataRepository;
import ai.mindslab.automl.backend.repository.ProjectSnapshotRepository;
import ai.mindslab.automl.backend.service.util.ParamConverterService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class TrainingService {
    private static final Logger logger = LoggerFactory.getLogger(TrainingService.class.getName());

    private final TenAiRestClient restClient;
    private final DataRestClient dataRestClient;

    // TODO: projectSnapshotService로 wrapping 하기로함.
    private final ProjectSnapshotRepository projectSnapshotRepository;
    private final ModelInTrainingService modelInTrainingService;
    private final GraphDataService graphDataService;
    private final ParamConverterService paramConverterService;
    private final GraphDataRepository graphDataRepository;


    @Value("${docker.image.brain}")
    private String dockerImage;

    /**
     * checkAvailableResources
     * 학습 서버에 현재 사용중이 아닌 (즉, 학습에 사용 가능한) 리소스 (GPU)가 하나라도 있는지 확인하는 기능
     *
     * @return boolean 사용 가능한 리소스가 하나라도 있으면 true, 하나도 없으면 false
     */
    public boolean checkAvailableResources() {
        logger.info("Checking for any available training resources...");
        List<Resource> list = Arrays.asList(restClient.getResources());

        return !(list.stream().allMatch(Resource::is_allocated));
    }

    /**
     * getAvailableResourcesSize
     * 학습 서버에 학습에 사용 가능한 리소스 (GPU)가 몇개 있는지 확인하는 기능
     *
     * @return int 학습에 사용 가능한 리소스 갯수 출력
     */
    public int getAvailableResourcesSize() {
        logger.info("Getting the number of available training resources...");
        List<Resource> list = Arrays.asList(restClient.getResources());
        List<Resource> filteredlist = list.stream().filter(resource -> !resource.is_allocated()).collect(Collectors.toList());

        return filteredlist.size();
    }

    /**
     * setTrainFormParams
     * 학습 서버에 학습 생성 요청을 보낼때 필요한 Post HttpRequest JSON body를 만들어주는 기능
     * ProjectSnapshot 내의 snapshot에서 정보를 읽어 요청 포맷에 맞추어 JSON을 매핑함
     *
     * @param snapshotDto snapshot에 담긴 정보를 파라미터화한 dto
     * @param envReqs     요청에 보내질 환경변수
     * @param volReqs     요청에 보내질 볼륨 리스트 (학습데이터, 노이즈데이터, 사전학습모델 등)
     */
    private void setTrainFormParams(ProjectSnapshotDto snapshotDto, List<EnvReq> envReqs, List<VolReq> volReqs) {
        logger.info("Configuring the parameters for create-training session request form...");
        logger.info("Importing the learning data list...");
        StringBuilder sb = new StringBuilder();
        List<Double> validRates = new ArrayList<>();

        snapshotDto.getLearningDataList().stream().
                forEach(learningData -> {
            String fileName = learningData.getAtchFileId();
            String filePath = learningData.getAtchFilePath();

            validRates.add(learningData.getTotFileCount() / 2.0);

            if (fileName == null) {
                fileName = UUID.randomUUID().toString();
            }

            if (fileName != null && filePath != null) {
                sb.append(fileName);
                sb.append(":");
                sb.append("/DATA/" + fileName);
                sb.append(",");

                volReqs.add(new VolReq(filePath, "/DATA/" + fileName));
            }
        });

        double validRate = 1.0 / validRates.stream().max(Double::compareTo).get();

        sb.deleteCharAt(sb.length() - 1);
        envReqs.add(new EnvReq("DATA_LIST", sb.toString()));

        logger.info("Importing the pre-trained model list...");

        // TODO: configure BASELINE if client has not specified a trained model
        envReqs.add(new EnvReq("BASELINE", "/BASELINE/" + snapshotDto.getPreTrainedModel().getMdlFileName()));
        volReqs.add(new VolReq(snapshotDto.getPreTrainedModel().getMdlFilePath(), "/BASELINE"));


        logger.info("Importing the noise data list...");
        StringBuilder sb2 = new StringBuilder();

        snapshotDto.getNoiseDataList().stream().forEach(noiseData -> {
            String fileName = noiseData.getAtchFileId();
            String filePath = noiseData.getAtchFilePath();

            if (fileName.isEmpty()) {
                fileName = UUID.randomUUID().toString();
            }

            if (fileName != null && filePath != null) {

                sb2.append("/NOISE/");
                sb2.append(fileName);
                sb2.append(",");

                volReqs.add(new VolReq(filePath, "/NOISE/" + fileName));
            }
        });
        sb2.deleteCharAt(sb2.length() - 1);
        envReqs.add(new EnvReq("NOISE_LIST", sb2.toString()));


        logger.info("Importing other training parameters...");
        envReqs.add(new EnvReq("LC_ALL", "C.UTF-8"));
        envReqs.add(new EnvReq("W2L_LANG", paramConverterService.convertLangCode(snapshotDto.getLanguage())));
        envReqs.add(new EnvReq("SAMPLERATE", paramConverterService.convertSampleRate(snapshotDto.getSampleRate())));
        envReqs.add(new EnvReq("MAX_DURATION", "600000"));
        envReqs.add(new EnvReq("VALIDATION_RATE", Double.toString(snapshotDto.getRate() / 100)));
        envReqs.add(new EnvReq("RESULT_DIR", "/MODEL"));
        envReqs.add(new EnvReq("USE_EOS", paramConverterService.convertEos(snapshotDto.getEos())));
        envReqs.add(new EnvReq("EPOCH", Integer.toString(snapshotDto.getEpochSize())));
        envReqs.add(new EnvReq("BATCH_SIZE", Integer.toString(snapshotDto.getBatchSize())));
        envReqs.add(new EnvReq("MIN_NOISE_SAMPLE", Integer.toString(snapshotDto.getMinNoiseSample())));
        envReqs.add(new EnvReq("MAX_NOISE_SAMPLE", Integer.toString(snapshotDto.getMaxNoiseSample())));
        envReqs.add(new EnvReq("MIN_SNR", Integer.toString(snapshotDto.getMinSnr())));
        envReqs.add(new EnvReq("MAX_SNR", Integer.toString(snapshotDto.getMaxSnr())));
    }

    /**
     * createTraining
     * 특정 projectSnapshot에 포함된 파라미터로 새로운 학습 세션을 생성
     * setTrainFormParams로 요청 JSON body를 생성해주고, 학습 서버에 학습 생성 Post Request를 전송
     * 학습 생성 후 DB의 ModelInTraining에도 기록해줌
     *
     * @param prjSnapshotId 학습과 연관된 projectSnapshot에 대한 id
     * @throws RuntimeException
     * @throws JsonProcessingException
     */
    public void createTraining(Long prjSnapshotId) throws RuntimeException, JsonProcessingException {
        logger.info("Creating new training session from ProjectSnapshotId \"{}\"...", prjSnapshotId);
        ProjectSnapshot projectSnapshot = projectSnapshotRepository.findById(prjSnapshotId).orElseThrow(() -> new EntityNotFoundException());

        String trainingId = projectSnapshot.getModelId();
        String json = projectSnapshot.getSnapshot();

        ObjectMapper objectMapper = new ObjectMapper();
        ProjectSnapshotDto snapshotDto = objectMapper.readValue(json, ProjectSnapshotDto.class);

        List<EnvReq> envReqs = new ArrayList<>();
        List<VolReq> volReqs = new ArrayList<>();
        setTrainFormParams(snapshotDto, envReqs, volReqs);

        List<String> commands = new ArrayList<>();

        TrainForm trainForm = TrainForm.builder().name(trainingId)
                .image(dockerImage)
                .command(commands)
                .env(envReqs)
                .volumes(volReqs)
                .build();

        logger.info("Snapshot mapped to trainForm {}", trainForm);

        try {
            logger.info("Sending the trainForm to the training server...");
            restClient.addModelToTrain(trainForm);
        } catch (Exception e) {
            logger.error("Error occurred while attempting to create a new training session. ProjectSnapshotId: {}", prjSnapshotId);
            projectSnapshotRepository.updateStatusByProjectSnapshotId("e", prjSnapshotId);
            throw RestTemplateException.builder().error(e.getMessage()).build();
        }
        logger.info("New training session successfully created. ProjectSnapshotId: {}; trainingId: {}", prjSnapshotId, trainForm.getName());
        projectSnapshotRepository.updateStatusByProjectSnapshotId("t", prjSnapshotId);
        logger.info("New training entity created in ModelInTraining Table...");
        modelInTrainingService.addModelForTraining(prjSnapshotId, snapshotDto.getEpochSize(), snapshotDto.getRegDt(), trainingId);
    }

    /**
     * checkRunningStatus
     * 진행중인 학습의 상태를 확인한 후, 조회한 상태에 따라 projectSnapshot에 상태메세지를 변경해주는 기능
     * 학습이 목표 epoch 달성으로 인해 자동 완료 되었다면 projectSnapshot에 "c" -> completed
     * 학습도중 오류가 발생한 경우 projectSnapshot에 "e" -> error를 기록해준다
     */
    public void checkRunningStatus() {
        logger.info("Checking status of on-going training sessions...");
        List<TrainedModel> trainingList = Arrays.asList(restClient.getTrainedModels());
        trainingList.forEach(t -> {
            try {
                String trainingId = t.getName();
                ModelInTraining modelInTraining = modelInTrainingService.getByTrainingId(trainingId);
                String status = t.getState().getType();

                if (modelInTraining != null) {
                    if (status.equals("Completed")) {
                        logger.info("Completed training found! Training name: {}", modelInTraining.getTrainingId());
                        modelInTrainingService.deleteBySnapshotId(modelInTraining.getProjectSnapshotId());
                        if (modelInTraining.getCurrEpoch() == 0) {
                            projectSnapshotRepository.updateStatusByProjectSnapshotId("e", modelInTraining.getProjectSnapshotId());
                            GraphData graphData = new GraphData();
                            graphData.setProjectSnapshotId(modelInTraining.getProjectSnapshotId());
                            graphData.setDataPoints("[]");
                            graphData.setFinalEpoch(0);

                            graphData.setBestWER(BigDecimal.valueOf(-1));
                            graphData.setBestWEREpoch(-1);
                            graphData.setBestTER(BigDecimal.valueOf(-1));
                            graphData.setBestTEREpoch(-1);

                            graphDataRepository.save(graphData);

                            logger.error("Training encountered a container error! Empty graphData saved and deleted from ModelInTraining!");
                        } else {
                            projectSnapshotRepository.updateStatusByProjectSnapshotId("c", modelInTraining.getProjectSnapshotId());
                            graphDataService.mergeGraphSnapshotByProjectSnapshotId(modelInTraining.getProjectSnapshotId(), modelInTraining.getMaxEpoch());

                            try {
                                postCompleteModelData(trainingId, modelInTraining.getProjectSnapshotId());
                            } catch (JsonProcessingException e) {
                                logger.error("Error: {}", e);
                            }

                            logger.info("Training \"{}\" was completed upto epoch {}. Successfully deleted from ModelInTraining!", modelInTraining.getTrainingId(), modelInTraining.getCurrEpoch());
                        }
                    } else if (status.equals("Error")) {
                        logger.info("Error occurred during training! Training name: {}", modelInTraining.getTrainingId());
                        projectSnapshotRepository.updateStatusByProjectSnapshotId("e", modelInTraining.getProjectSnapshotId());
                        modelInTrainingService.deleteBySnapshotId(modelInTraining.getProjectSnapshotId());
                        graphDataService.mergeGraphSnapshotByProjectSnapshotId(modelInTraining.getProjectSnapshotId(), modelInTraining.getMaxEpoch());
                    }
                }
                else {
                    logger.info("Training Id: \"{}\" is not in model-in-training. No further action!", trainingId);
                }

            } catch (Exception e) {
                logger.error("Epoch check error. Training name: {}", t.getName());
            }
        });
    }

    /**
     * endTraining
     * 유저가 학습 종료를 요청했을시 학습 중단을 해주는 기능
     * 학습 중단 후 projectSnapshot에 "c" -> complete 상태메세지를 기록해줌
     *
     * @param trainingId        화면설계서상 학습 모델 아이디 "mdl_userID_엔진타입_생성일시"
     * @param projectSnapshotId 학습과 연관된 projectSnapshot에 대한 id
     * @throws RuntimeException
     */
    public void endTraining(String trainingId, Long projectSnapshotId) throws RuntimeException, JsonProcessingException {
        try {
            postCompleteModelData(trainingId, projectSnapshotId);
        } catch (JsonProcessingException e) {
            logger.error("Error: {}", e);
        } catch (IndexOutOfBoundsException e) {
            throw new IndexOutOfBoundsException("Trained model files not found");
        } finally {
            restClient.endTraining(trainingId);
        }

        projectSnapshotRepository.updateStatusByProjectSnapshotId("c", projectSnapshotId);

        int currEpoch = modelInTrainingService.getCurrEpochByProjectSnapshotId(projectSnapshotId);

        try {
            modelInTrainingService.deleteBySnapshotId(projectSnapshotId);
        } catch (Exception e) {
            logger.error("[endTraining] Error while deleting model in training service by projectSnapshotId : {}", projectSnapshotId);
        }
        int finalEpoch = graphDataService.readLogFromEpoch(projectSnapshotId, trainingId, currEpoch + 1);

        if (finalEpoch == 0) {
            GraphData graphData = new GraphData();
            graphData.setProjectSnapshotId(projectSnapshotId);
            graphData.setDataPoints("[]");
            graphData.setFinalEpoch(0);

            graphData.setBestWER(BigDecimal.valueOf(-1));
            graphData.setBestWEREpoch(-1);
            graphData.setBestTER(BigDecimal.valueOf(-1));
            graphData.setBestTEREpoch(-1);

            graphDataRepository.save(graphData);
            logger.info("Graph data successfully saved when current epoch is 0! graphData: {}", graphData);
        } else {
            graphDataService.mergeGraphSnapshotByProjectSnapshotId(projectSnapshotId, finalEpoch);
        }
    }

    /**
     * postCompleteModelData
     * 사용자 학습 모델 데이터 정보 등록 요청
     *
     * @param trainingId
     * @param projectSnapshotId
     * @throws JsonProcessingException
     */
    public void postCompleteModelData(String trainingId, Long projectSnapshotId) throws JsonProcessingException {
        String projectType = projectSnapshotRepository.findProjectTypeByProjectSnapshotId(projectSnapshotId);
        String userEmail = projectSnapshotRepository.findUserEmailByModelId(trainingId);
        String snapshot = projectSnapshotRepository.findSnapshotByProjectSnapshotId(projectSnapshotId);

        String sampleRate = "";
        String langCd = "";

        ObjectMapper objectMapper = new ObjectMapper();
        ProjectSnapshotDto snapshotDto = objectMapper.readValue(snapshot, ProjectSnapshotDto.class);

        if (snapshotDto.getSampleRate().equals("SR001")) {
            sampleRate = "SR001";
        } else {
            sampleRate = "SR002";
        }

        if (snapshotDto.getLanguage().equals("LN_KO")) {
            langCd = "LN_KO";
        } else {
            langCd = "LN_EN";
        }

        TrainedModelFile[] files = restClient.getTrainedModelFiles(trainingId);

        if (files.length == 0) {
            throw new IndexOutOfBoundsException("Trained model files not found");
        }

        for (TrainedModelFile file : files) {
            String modelFilePath = file.getPath();
            int lastDirPos = modelFilePath.lastIndexOf('/');

            ModelForm modelForm = ModelForm.builder()
                    .userId(userEmail)
                    .mdlId(trainingId)
                    .engnType("EN_" + projectType)
                    .mdlFilePath(modelFilePath.substring(0, lastDirPos))
                    .langCd(langCd)
                    .smplRate(sampleRate)
                    .useEosYn(snapshotDto.getEos())
                    .build();

            dataRestClient.addModelData(modelForm);
        }
    }

    public StateMessage getModelStatusByTrainingId(String name) {
        return restClient.getTrainedModel(name).getState();
    }
}
