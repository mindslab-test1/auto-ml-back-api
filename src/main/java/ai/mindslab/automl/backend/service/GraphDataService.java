package ai.mindslab.automl.backend.service;

import ai.mindslab.automl.backend.client.TenAiRestClient;
import ai.mindslab.automl.backend.client.model.response.tenai.EpochLog;
import ai.mindslab.automl.backend.client.model.response.tenai.TrainedModelLog;
import ai.mindslab.automl.backend.model.dto.training.GraphDataDto;
import ai.mindslab.automl.backend.model.dto.training.GraphDataPointDto;
import ai.mindslab.automl.backend.model.entity.project.GraphData;
import ai.mindslab.automl.backend.model.entity.project.GraphDataSnapshot;
import ai.mindslab.automl.backend.model.entity.project.ModelInTraining;
import ai.mindslab.automl.backend.repository.GraphDataRepository;
import ai.mindslab.automl.backend.repository.GraphDataSnapshotRepository;
import ai.mindslab.automl.backend.repository.ModelInTrainingRepository;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class GraphDataService {
    private final GraphDataRepository graphDataRepository;
    private final GraphDataSnapshotRepository graphDataSnapshotRepository;
    private final ModelInTrainingService modelInTrainingService;
    private final TenAiRestClient restClient;
    private static final Logger logger = LoggerFactory.getLogger(GraphDataService.class.getName());
    private final ModelInTrainingRepository modelInTrainingRepository;

    /**
     * getAllByProjectSnapshotId
     * projectSnapshotId에 속한 그래프 데이터를 전부 가져온다.
     *
     * @param projectSnapshotId 학습과 연관된 projectSnapshot에 대한 id
     * @return List<GraphData> list of graph data
     */
    public List<GraphData> getAllByProjectSnapshotId(Long projectSnapshotId) {
        logger.info("Getting all graph data linked to projectSnapshotId: {}", projectSnapshotId);
        return graphDataRepository.findAllByProjectSnapshotId(projectSnapshotId);
    }

    /**
     * updateGraphData()
     * 현재 진행중인 학습중 에러 그래프 데이터의 업데이트가 필요한 학습들을 골라서 readLogFromEpoch 메소드로 처리한다.
     * 학습 에러 그래프가 업데이트가 필요한지에 대한 결정은 현재까지 기록한 그래프 데이터가 목표 epoch에 미달 하였는지로 결정한다.
     */
    public void updateGraphData() {
        logger.info("Determining which in-progress training sessions require graph data update...");
        List<ModelInTraining> list = modelInTrainingService.getAll();
        list.forEach(m -> {
            try {
                if (m.getCurrEpoch() == m.getMaxEpoch()) {
                    logger.info("Training \"{}\" has already reached its target epoch. Skipping graph data update.", m.getTrainingId());
                } else {
                    int latestEpoch = readLogFromEpoch(m.getProjectSnapshotId(), m.getTrainingId(), m.getCurrEpoch() + 1);
                    logger.info("Updating currEpoch for training \"{}\"", m.getTrainingId());
                    m.setCurrEpoch(latestEpoch);
                    modelInTrainingRepository.save(m);
                }
            } catch (Exception e) {
                logger.error("Error occurred while updating graph for {}", m.getTrainingId());
            }
        });
    }

    /**
     * readLogFromEpoch
     * 특정 학습의 특정 epoch로부터 로그 내역을 조회하여, 로그 내부의 그래프 정보를 가져와 GraphData 테이블에 저장해준다.
     *
     * @param projectSnapshotId 학습과 연관된 projectSnapshot에 대한 id
     * @param trainingId        학습 서버에서 사용되는 학습 이름 (화면설계서 모델 아이디 "mdl_userID_엔진타입_생성일시")
     * @param epoch             학습 내 몇번째 epoch부터 로그를 불러올지 설정
     * @return int updatedEpoch
     */
    @Transactional
    public int readLogFromEpoch(Long projectSnapshotId, String trainingId, int epoch) throws JsonProcessingException {
        logger.info("Getting log info for trainingId \"{}\" from epoch #{}", trainingId, epoch);
        TrainedModelLog[] trainedModelLogs = restClient.getTrainedModelLogs(trainingId, epoch);

        if (trainedModelLogs.length == 0) {
            logger.info("No further graph data for \"{}\" exists.", trainingId);

            return epoch - 1;
        } else {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);


            for (int i = 0; i < trainedModelLogs.length; i++) {
                TrainedModelLog trainedModelLog = trainedModelLogs[i];
                EpochLog log = mapper.readValue(trainedModelLog.getLog(), EpochLog.class);
                int currEpoch = trainedModelLog.getEpoch();

                GraphDataSnapshot graphDataSnapshot = new GraphDataSnapshot();
                GraphDataPointDto lossData = new GraphDataPointDto();
                GraphDataPointDto terData = new GraphDataPointDto();
                GraphDataPointDto werData = new GraphDataPointDto();

                try {
                    graphDataSnapshot.setEpoch(currEpoch);

                    lossData.setGraphType("loss");
                    lossData.setTrain(new BigDecimal(log.getLoss()));

                    terData.setGraphType("TER");
                    terData.setTrain(new BigDecimal(log.getTrain_TER()));

                    werData.setGraphType("WER");
                    werData.setTrain(new BigDecimal(log.getTrain_WER()));
                } catch (NumberFormatException e) {
                    logger.error("[readLogFromEpoch] Unable to set the value to BigDecimal. Error: {}, {}, {}",
                            log.getLoss(), log.getTrain_TER(), log.getTrain_WER());
                }


                Map<String, String> devErrors = log.getDev_errors();

                BigDecimal devTotalWER = new BigDecimal(0);
                BigDecimal devTotalTER = new BigDecimal(0);

                logger.info("[readLogFromEpoch] Mapping by line Type...");
                // Mapping by line type
                try {
                    for (String key : devErrors.keySet()) {

                        String[] readKeyName = key.split("-");
                        String graphType = readKeyName[2];
                        String graphLine = readKeyName[0] + "-" + readKeyName[1];

                        if (graphType.equals("loss")) {
                            lossData.setDevError(graphLine, devErrors.get(key));
                        } else if (graphType.equals("WER")) {
                            werData.setDevError(graphLine, devErrors.get(key));

                            devTotalWER = new BigDecimal(devErrors.get(key));
                            //devTotalWER < bestWERScore 일 때, graphDataSnapshot에 저장
                            if (devTotalWER.compareTo(graphDataSnapshot.getBestWERScore()) == -1) {
                                graphDataSnapshot.setBestWERScore(devTotalWER);
                            }

                        } else if (graphType.equals("TER")) {
                            terData.setDevError(graphLine, devErrors.get(key));

                            devTotalTER = new BigDecimal(devErrors.get(key));
                            //devTotalTER < bestTERScore 일 때, graphDataSnapshot에 저장
                            if (devTotalTER.compareTo(graphDataSnapshot.getBestTERScore()) == -1) {
                                graphDataSnapshot.setBestTERScore(devTotalTER);
                            }
                        }
                    }
                } catch (Exception e) {
                    logger.error("[readLogFromEpoch] Error: {}", e.getMessage());
                }


                List<GraphDataPointDto> graphDataPointDtoList = new ArrayList<>();
                graphDataPointDtoList.add(lossData);
                graphDataPointDtoList.add(terData);
                graphDataPointDtoList.add(werData);
                GraphDataDto graphDataDto = new GraphDataDto();
                graphDataDto.setEpoch(currEpoch);
                graphDataDto.setData(graphDataPointDtoList);

                String json = "";
                try {
                    json = mapper.writeValueAsString(graphDataDto);
                } catch (JsonProcessingException e) {
                    logger.error("Error converting to JSON string {}", graphDataDto);
                }
                graphDataSnapshot.setProjectSnapshotId(projectSnapshotId);
                graphDataSnapshot.setDataPoints(json);

                graphDataSnapshotRepository.save(graphDataSnapshot);
            }

            int updatedEpoch = trainedModelLogs[trainedModelLogs.length - 1].getEpoch();
            logger.info("Graph data successfully updated! TrainingId: {}, from epoch \"{}\" to epoch \"{}\"", trainingId, epoch, updatedEpoch);
            return updatedEpoch;
        }
    }

    /**
     * deleteAllByProjectSnapshotId
     * (현재 미사용) 특정 projectSnapshotId에 연관된 그래프 데이터를 전부 조회하여 삭제해준다.
     *
     * @param projectSnapshotId 학습과 연관된 projectSnapshot에 대한 id
     */
    public void deleteAllByProjectSnapshotId(Long projectSnapshotId) {
        logger.info("Deleting all graph data linked to projectSnapshotId: {}", projectSnapshotId);
        List<GraphData> list = getAllByProjectSnapshotId(projectSnapshotId);
        list.forEach(graphDataRepository::delete);
    }

    @Transactional
    public void mergeGraphSnapshotByProjectSnapshotId(Long projectSnapshotId, int finalEpoch) {
        logger.info("Merging graphSnapshot info for projectSnapshotId \"{}\" with finalEpoch {}", projectSnapshotId, finalEpoch);
        List<GraphDataSnapshot> list = graphDataSnapshotRepository.findAllByProjectSnapshotIdOrderByEpoch(projectSnapshotId);

        if (!list.isEmpty()) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("[");
            list.forEach(b -> {
                stringBuilder.append(b.getDataPoints());
                if (b.getEpoch() < finalEpoch) {
                    stringBuilder.append(",");
                }
            });


            stringBuilder.append("]");

            // graphDataSnapshot의 bestWERScore & bestTERScore 데이터들 중에서 minimum score, 즉 베스트값을 가져온다.
            GraphData graphData = new GraphData();
            try {
                GraphDataSnapshot tempWER = Collections.min(list, Comparator.comparing(GraphDataSnapshot::getBestWERScore));
                GraphDataSnapshot tempTER = Collections.min(list, Comparator.comparing(GraphDataSnapshot::getBestTERScore));
                graphData.setProjectSnapshotId(projectSnapshotId);
                graphData.setDataPoints(stringBuilder.toString());
                graphData.setFinalEpoch(finalEpoch);

                graphData.setBestWER(tempWER.getBestWERScore());
                graphData.setBestWEREpoch(tempWER.getEpoch());
                graphData.setBestTER(tempTER.getBestTERScore());
                graphData.setBestTEREpoch(tempTER.getEpoch());

                graphDataRepository.save(graphData);
            } catch (Exception e) {
                logger.error("[mergeGraphSnapshotByProjectSnapshotId] Error while setting graphData : {}", graphData);
            }

            graphDataSnapshotRepository.deleteAllByProjectSnapshotId(projectSnapshotId);
            logger.info("Graph data successfully merged! graphData: {}", graphData);

        }
    }
}
