package ai.mindslab.automl.backend.service.util;

import com.google.common.io.Files;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

@Slf4j
@Service
@RequiredArgsConstructor
public class FfmpegService {
    private static final Logger logger = LoggerFactory.getLogger(FfmpegService.class.getName());


    /**
     * convertSampleRate
     * 받아오는 MultipartFile을 ffmpeg 사용하여 SampleRate를 바꿔주는 작업.
     *
     * @param mpFile SampleRate 변경이 필요한 MultipartFile 타입.
     * @param sampleRate 변경요청 sampleRate 값.
     * @return convertedFile ffmpeg로 SampleRate가 변경된 File 타입.
     */
    public File convertSampleRate(MultipartFile mpFile, int sampleRate) throws IOException {

        File tempDir = Files.createTempDir();
        String inputFile = tempDir.getPath() + "/" + mpFile.getOriginalFilename() + ".wav";
        mpFile.transferTo(Paths.get(inputFile));
        String outputFile = tempDir.getPath() + "/" + "(SR"+ sampleRate +")_" + mpFile.getOriginalFilename() + ".wav";
        try {
            String ffmpegCommand = FfmpegUtils.ffmpegConvertSampleRate(inputFile, outputFile, sampleRate);
            Process convertingProcess = Runtime.getRuntime().exec(ffmpegCommand);
            convertingProcess.waitFor();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        File convertedFile = new File(outputFile);
        return convertedFile;
    }

    /**
     * deleteTempDir
     * 임시 디렉토리 및 저장된 파일 삭제하는 작업.
     *
     * @param convertedFile SampleRate 변경된 파일.
     */
    public void deleteTempDir(File convertedFile) throws IOException {
        String path = convertedFile.getParentFile().getAbsolutePath();

        File fileDirectory = new File(path);
        FileUtils.deleteDirectory(fileDirectory);

    }

}