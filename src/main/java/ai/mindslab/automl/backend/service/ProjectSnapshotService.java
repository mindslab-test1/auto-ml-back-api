package ai.mindslab.automl.backend.service;

import ai.mindslab.automl.backend.model.entity.project.ProjectSnapshot;
import ai.mindslab.automl.backend.repository.ProjectSnapshotRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProjectSnapshotService {
    private final ProjectSnapshotRepository projectSnapshotRepository;

    public void delete (Long projectSnapshotId) {
        Optional<ProjectSnapshot> optional = projectSnapshotRepository.findById(projectSnapshotId);
        if (optional.isPresent()) {
            projectSnapshotRepository.deleteById(projectSnapshotId);
        }
        else {
            throw new EmptyResultDataAccessException(0);
        }
    }

    public Optional<ProjectSnapshot> getOptional (Long projectSnapshotId){
        return projectSnapshotRepository.findById(projectSnapshotId);
    }
}
