package ai.mindslab.automl.backend.service;

import ai.mindslab.automl.backend.client.TenAiRestClient;
import ai.mindslab.automl.backend.client.exception.RestTemplateException;
import ai.mindslab.automl.backend.client.model.request.DeployForm;
import ai.mindslab.automl.backend.client.model.request.EnvReq;
import ai.mindslab.automl.backend.client.model.request.VolReq;
import ai.mindslab.automl.backend.client.model.response.tenai.DeployedModel;
import ai.mindslab.automl.backend.model.dto.deploy.DeploySnapshotCreateDto;
import ai.mindslab.automl.backend.model.dto.deploy.DeploySnapshotDto;
import ai.mindslab.automl.backend.model.entity.project.DeploySnapshot;
import ai.mindslab.automl.backend.repository.DeploySnapshotRepository;
import ai.mindslab.automl.backend.repository.ProjectSnapshotRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class DeployService {
    private static final Logger logger = LoggerFactory.getLogger(DeployService.class.getName());

    private final TenAiRestClient restClient;
    private final DeploySnapshotRepository deploySnapshotRepository;
    private final ProjectSnapshotRepository projectSnapshotRepository;

    @Value("${docker.image.deploy}")
    private String dockerImageDeploy;

    /**
     * setDeployFormParams
     * 학습된 모델을 배포 서버에 배포 생성 요청하기 위해 POST request 요청 파라미터를 만들어줌.
     *
     * @param snapshotDto 배포 생성에 필요한 파라미터 정보가 들어간 object. am 학습모델의 경로와 이름, 그리고 lm 모델의
     *                    언어, 샘플링레이트, eos 사용 여부가 들어가있음.
     * @param envReqs     배포 생성에 필요한 환경 변수 파라미터 목록. 처음엔 비어있음.
     * @param volReqs     배포 생성시 생성되는 컨테이너 환경에 첨부될 볼륨 정보 목록. 처음엔 비어있음.
     * @param commands    배포 생성 요청시 명시해야 하는 명령어 목록. 처음엔 비어있음.
     */
    private void setDeployFormParams(DeploySnapshotDto snapshotDto, List<EnvReq> envReqs, List<VolReq> volReqs, List<String> commands) {
        logger.info("Configuring the parameters for create-training session request form...");

        commands.add("--vadremote=114.108.173.124:32001");
        commands.add("--nthread_decoder=75");
        commands.add("--usedetailsegment=false");
        commands.add("--am=/AM/" + snapshotDto.getAmFileName());

        envReqs.add(new EnvReq("LC_ALL", "C.UTF-8"));

        String eos = "";
        if (snapshotDto.getEos().equals("Y")) {
            eos = "eos";
        }
        // TODO: Find out where lm model host_path comes from
        volReqs.add(new VolReq(String.format("/AUTOML/example/base_%s_%s_%s",
                snapshotDto.getLanguage(),
                Integer.parseInt(snapshotDto.getSampleRate()) / 1000 + "k",
                eos),
                "/MODEL"));
        volReqs.add(new VolReq(snapshotDto.getAmFilePath(), "/AM"));
    }

    /**
     * createDeploy
     * DeploySnapshot에 저장된 배포 생성 파라미터를 불러와 배포 생성 요청을 만든 후, 요청을 배포 서버에 보내준다.
     *
     * @param deploySnapshotId 배포 생성 요청을 위한 파라미터 정보가 담긴 DeploySnapshot 객체의 ID.
     */
    public void createDeploy(Long deploySnapshotId) throws RuntimeException, JsonProcessingException {
        logger.info("Creating new training session from ProjectSnapshotId \"{}\"...", deploySnapshotId);
        DeploySnapshot deploySnapshot = deploySnapshotRepository.findById(deploySnapshotId).orElse(null);
        String json = deploySnapshot.getSnapshot();

        ObjectMapper objectMapper = new ObjectMapper();
        DeploySnapshotDto snapshotDto = objectMapper.readValue(json, DeploySnapshotDto.class);

        List<EnvReq> envReqs = new ArrayList<>();
        List<VolReq> volReqs = new ArrayList<>();
        List<String> commands = new ArrayList<>();
        setDeployFormParams(snapshotDto, envReqs, volReqs, commands);

        DeployForm deployForm = DeployForm.builder().name(deploySnapshot.getDeployId())
                .image(dockerImageDeploy)
                .command(commands)
                .container_port(15001)
                .env(envReqs)
                .volumes(volReqs)
                .build();

        logger.info("Snapshot mapped to postForm {}", deployForm);
        try {
            logger.info("Sending the postForm to the deploying server...");
            restClient.addModelToDeploy(deployForm);
        } catch (Exception e) {
            logger.error("Error occurred while attempting to deploy a new API. DeploySnapshotId: {}", deploySnapshotId);
            // TODO: ERROR MESSAGE FOR DEPLOY?
            throw RestTemplateException.builder().error(e.getMessage()).build();
        }
        logger.info("New API successfully deployed. deploySnapshotId: {}; trainingId: {}", deploySnapshotId, deployForm.getName());
        // TODO: UPDATE STATUS TO "RUNNING"?
    }

    /**
     * getDeployEndpoint
     * 모델 배포가 배포 서버에 생성된 후, 배포 실행중 사용되는 host와 port를 주소값으로 변경하여 database에 저장해줌.
     *
     * @param deploySnapshotId 생성된 모델 배포와 연관된 DeploySnapshot entity의 ID.
     */
    public String getDeployEndpoint(Long deploySnapshotId) {
        try {
            DeploySnapshot deploySnapshot = deploySnapshotRepository.findById(deploySnapshotId).orElse(null);
            String deployId = "";
            if (deploySnapshot != null) {
                deployId = deploySnapshot.getDeployId();
                logger.info("Sending the get request to the deploying server...");
                DeployedModel deployedModel = restClient.getDeployedModel(deployId);
                deploySnapshot.setEndpoint(deployedModel.getDeploy_host() + ":" + deployedModel.getDeploy_port());
                deploySnapshot = deploySnapshotRepository.save(deploySnapshot);
                logger.info("API endpoint for the deployed model successfully updated! DeployedSnapshotId: {}", deploySnapshotId);
                return deploySnapshot.getEndpoint();
            } else {
                logger.error("DeploySnapshot was not found in the database. DeployedSnapshotId: {}", deploySnapshotId);
                return null;
            }
        } catch (Exception e) {
            logger.error("Error occurred while attempting to update the API endpoint for the deployed model. DeploySnapshotId: {}", deploySnapshotId);
            // TODO: ERROR MESSAGE FOR DEPLOY?
            throw RestTemplateException.builder().error(e.getMessage()).build();
        }
    }

    /**
     * addDeploySnapshot
     * 배포 생성을 위해 필요한 deploySnapshot 객체를 생성해주는 기능.
     *
     * @param dto DeploySnapshot을 생성하기 위한 정보가 담긴 JSON Body (POST request body)
     * @return DeploySnapshot 생성된 deploySnapshot
     * @throws JsonProcessingException
     */
    public DeploySnapshot addDeploySnapshot(DeploySnapshotCreateDto dto) throws JsonProcessingException {
        logger.info("Creating new deploySnapshot entity for ModelId : {}, ProjectSnapshotId : {}...", dto.getModelId(), dto.getProjectSnapshotId());
        DeploySnapshot snapshot = new DeploySnapshot();
        snapshot.setProjectSnapshotId(dto.getProjectSnapshotId());

        try {
            Long userId = projectSnapshotRepository.findUserIdByModelId(dto.getModelId());
            String projectType = projectSnapshotRepository.findProjectTypeByProjectSnapshotId(dto.getProjectSnapshotId());
            snapshot.setDeployId(userId + "_" + projectType + "_" +
                    LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")) + "_" + dto.getProjectSnapshotId());
        } catch (Exception e) {
            logger.error("Error occurred while attempting to set the snapshot deployId.");
        }

        DeploySnapshotDto snapshotDto = new DeploySnapshotDto();

        snapshotDto.setAmFileName(dto.getAmFileName());
        snapshotDto.setAmFilePath(dto.getAmFilePath());
        snapshotDto.setLanguage(dto.getLanguage());
        snapshotDto.setSampleRate(dto.getSampleRate());
        snapshotDto.setEos(dto.getEos());

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(snapshotDto);
        snapshot.setSnapshot(json);
        logger.info("DeploySnapshot is successfully created! DeployedSnapshot: {}", snapshotDto);
        return deploySnapshotRepository.save(snapshot);
    }
}
