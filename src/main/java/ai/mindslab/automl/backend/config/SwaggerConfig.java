package ai.mindslab.automl.backend.config;

import ai.mindslab.automl.backend.payload.ErrorResponse;
import ai.mindslab.automl.backend.payload.TestModelResult;
import com.fasterxml.classmate.TypeResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;


//@EnableSwagger2WebMvc
//@Import(SpringDataRestConfiguration.class)
@Configuration
public class SwaggerConfig {
    @Bean
    public Docket api() {
        TypeResolver typeResolver = new TypeResolver();
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("ai.mindslab.automl.backend.controller"))
                .paths(PathSelectors.any())
                .build()
                .additionalModels(typeResolver.resolve(TestModelResult.class))
                .additionalModels(typeResolver.resolve(ErrorResponse.class))
                .useDefaultResponseMessages(false) ;
    }
}