package ai.mindslab.automl.backend.config;

import ai.mindslab.automl.backend.client.exception.RestTemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import static org.springframework.http.HttpStatus.Series.CLIENT_ERROR;
import static org.springframework.http.HttpStatus.Series.SERVER_ERROR;

public class CustomClientErrorHandler implements ResponseErrorHandler {

    private final Logger LOG = LoggerFactory.getLogger(CustomClientErrorHandler.class);

    @Override
    public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
        return (clientHttpResponse.getStatusCode().series() == CLIENT_ERROR
                || clientHttpResponse.getStatusCode().series() == SERVER_ERROR);
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        String errorMssg = "";
        try {
            if (response != null && response.getBody() != null) {
                StringBuilder inputStringBuilder = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getBody(), StandardCharsets.UTF_8));
                String line = bufferedReader.readLine();
                while (line != null) {
                    inputStringBuilder.append(line);
                    inputStringBuilder.append('\n');
                    line = bufferedReader.readLine();
                }
                errorMssg = inputStringBuilder.toString();
                LOG.error("Error response : {}", errorMssg);
            }
        } catch (IOException e) {
            LOG.error("CustomClientErrorHandler | HTTP Status Code: " + response.getStatusCode().value());
            throw new RestTemplateException(response.getStatusCode().value(), "");
        }


        if (response.getStatusCode().series() == SERVER_ERROR) {
            LOG.error("CustomClientErrorHandler | HTTP Status Code: " + response.getStatusCode().value());
            throw new RestTemplateException(response.getStatusCode().value(), errorMssg);
        } else if (response.getStatusCode().series() == CLIENT_ERROR) {
            LOG.error("CustomClientErrorHandler | HTTP Status Code: " + response.getStatusCode().value());
            throw new RestTemplateException(response.getStatusCode().value(), errorMssg);

        }

    }
}
