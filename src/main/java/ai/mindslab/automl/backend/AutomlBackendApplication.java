package ai.mindslab.automl.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutomlBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(AutomlBackendApplication.class, args);
    }

}
