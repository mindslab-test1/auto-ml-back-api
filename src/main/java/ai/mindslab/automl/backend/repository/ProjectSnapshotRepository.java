package ai.mindslab.automl.backend.repository;

import ai.mindslab.automl.backend.model.entity.project.ProjectSnapshot;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProjectSnapshotRepository extends CrudRepository<ProjectSnapshot, Long> {
    List<ProjectSnapshot> findAllByProjectIdOrderByRegDtDesc(Long id);

    Optional<ProjectSnapshot> findById(Long id);

    @Query(value = "SELECT u.email FROM project_snapshot ps JOIN project p ON p.id = ps.project_id  JOIN user u ON p.user_id = u.id WHERE ps.model_id = ?1",
            nativeQuery = true)
    String findUserEmailByModelId(String modelId);

    @Query(value = "SELECT u.id FROM project_snapshot ps JOIN project p ON p.id = ps.project_id  JOIN user u ON p.user_id = u.id WHERE ps.model_id = ?1",
            nativeQuery = true)
    Long findUserIdByModelId(String modelId);

    @Query(value = "SELECT p.project_type FROM project_snapshot ps JOIN project p ON p.id = ps.project_id WHERE ps.id = ?1",
            nativeQuery = true)
    String findProjectTypeByProjectSnapshotId(Long id);
    
    @Query(value = "SELECT p.language FROM project_snapshot ps JOIN project_stt p ON p.id = ps.project_id WHERE ps.id = ?1",
            nativeQuery = true)
    String findLanguageByProjectSnapshotId(Long id);

    @Query("SELECT p.snapshot FROM ProjectSnapshot p WHERE p.id = ?1")
    String findSnapshotByProjectSnapshotId(Long id);

    @Query("SELECT p.snapshot FROM ProjectSnapshot p WHERE p.modelId = ?1")
    String findSnapshotByProjectModelId(String modelid);

    @Transactional
    @Modifying
    @Query("UPDATE ProjectSnapshot prjSnp SET prjSnp.status = ?1 WHERE prjSnp.id = ?2")
    void updateStatusByProjectSnapshotId(String status, Long projectSnapshotId);
}
