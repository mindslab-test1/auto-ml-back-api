package ai.mindslab.automl.backend.repository;

import ai.mindslab.automl.backend.model.entity.project.DeploySnapshot;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeploySnapshotRepository extends CrudRepository<DeploySnapshot, Long> {
    DeploySnapshot findByProjectSnapshotId(Long id);
}
