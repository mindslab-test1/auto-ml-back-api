package ai.mindslab.automl.backend.repository;

import ai.mindslab.automl.backend.model.entity.project.Queue;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface QueueRepository extends CrudRepository<Queue,Long> {
    Optional<Queue> findBySnapshotId (Long snapshotId);
    List<Queue> findAllByOrderByRegDtAsc();
    List<Queue> findAll();
}
