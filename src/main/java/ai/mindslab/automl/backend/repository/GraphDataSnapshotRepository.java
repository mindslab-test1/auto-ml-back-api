package ai.mindslab.automl.backend.repository;

import ai.mindslab.automl.backend.model.entity.project.GraphDataSnapshot;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GraphDataSnapshotRepository extends CrudRepository<GraphDataSnapshot, Long> {
    List<GraphDataSnapshot> findAllByProjectSnapshotIdOrderByEpoch (Long projectSnapshotId);

    void deleteAllByProjectSnapshotId (Long projectSnapshotId);
}
