package ai.mindslab.automl.backend.repository;

import ai.mindslab.automl.backend.model.entity.project.ModelTestSnapshot;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ModelTestSnapshotRepository extends CrudRepository<ModelTestSnapshot, Long> {
    @Modifying
    @Transactional
    @Query("UPDATE ModelTestSnapshot SET result = ?2 WHERE id = ?1 ")
    void updateResultById(Long id, String result);
}
