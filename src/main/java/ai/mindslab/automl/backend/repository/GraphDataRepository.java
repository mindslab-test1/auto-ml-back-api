package ai.mindslab.automl.backend.repository;

import ai.mindslab.automl.backend.model.entity.project.GraphData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GraphDataRepository extends CrudRepository<GraphData,Long> {
    List<GraphData> findAllByProjectSnapshotId(Long projectSnapshotId);
}
