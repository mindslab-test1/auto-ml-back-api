package ai.mindslab.automl.backend.repository;

import ai.mindslab.automl.backend.model.entity.project.ModelInTraining;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ModelInTrainingRepository extends CrudRepository<ModelInTraining, Long> {
    Optional<ModelInTraining> findByProjectSnapshotId(Long projectSnapshotId);
    Optional<ModelInTraining> findByTrainingId(String trainingId);
}
