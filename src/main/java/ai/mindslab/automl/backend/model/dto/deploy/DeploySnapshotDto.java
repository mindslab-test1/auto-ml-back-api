package ai.mindslab.automl.backend.model.dto.deploy;

import lombok.Data;

@Data
public class DeploySnapshotDto {
    private String amFilePath;
    private String amFileName;
    private String language;
    private String sampleRate;
    private String eos;
}
