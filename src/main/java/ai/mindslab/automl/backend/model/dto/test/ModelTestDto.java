package ai.mindslab.automl.backend.model.dto.test;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ModelTestDto {
    private String amFilePath;
    private String amFileName;
    private String dataFilePath;
    private String dataFileName;
    private String modelId;
}
