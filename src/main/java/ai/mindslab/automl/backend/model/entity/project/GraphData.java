package ai.mindslab.automl.backend.model.entity.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "graph_data")
public class GraphData {
    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long projectSnapshotId;

    @Lob
    private String dataPoints;

    private int finalEpoch;

    private int bestWEREpoch;
    private BigDecimal bestWER;

    private int bestTEREpoch;
    private BigDecimal bestTER;

    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    private LocalDateTime regDt;
}
