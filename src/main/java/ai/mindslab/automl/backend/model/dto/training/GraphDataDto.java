package ai.mindslab.automl.backend.model.dto.training;

import lombok.Data;

import java.util.List;

@Data
public class GraphDataDto {
    private int epoch;
    private List<GraphDataPointDto> data;
}
