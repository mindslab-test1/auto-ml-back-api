package ai.mindslab.automl.backend.model.dto.project;

import lombok.Data;

@Data
public class PreTrainedModel {
    private String mdlId;
    private String mdlFileName;
    private String mdlFilePath;
}
