package ai.mindslab.automl.backend.model.dto.project;

import lombok.Data;

@Data
public class NoiseData {
    private String atchFileId;
    private String atchFileDisp;
    private String atchFilePath;
    private String atchFileName;
    private String orgFileName;
    private String atchFileType;
    private String atchFileSize;
    private String smplRate;
    private Long playSec;
    private Long playSecAvg;
    private Long minPlaySec;
    private Long maxPlaySec;
    private int totFileCount;
    private String langCd;
    private String useEosYn;
    private String regDt;
    private String engnType;
}
