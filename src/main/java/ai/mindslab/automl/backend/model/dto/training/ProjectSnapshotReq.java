package ai.mindslab.automl.backend.model.dto.training;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ProjectSnapshotReq {
    @ApiModelProperty(value = "Snapshot ID")
    private Long snapshotId;

    @ApiModelProperty(value = "학습 모델 ID")
    private String trainingId;
}
