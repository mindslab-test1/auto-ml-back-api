package ai.mindslab.automl.backend.model.entity.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "project_snapshot")
public class ProjectSnapshot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Lob
    private String snapshot;

    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime regDt;

    private Long projectId;

    private String status;

    private String modelId;
}