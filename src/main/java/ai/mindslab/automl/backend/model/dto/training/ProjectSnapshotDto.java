package ai.mindslab.automl.backend.model.dto.training;

import ai.mindslab.automl.backend.model.dto.project.LearningData;
import ai.mindslab.automl.backend.model.dto.project.NoiseData;
import ai.mindslab.automl.backend.model.dto.project.PreTrainedModel;
import lombok.Data;

import java.util.List;

@Data
public class ProjectSnapshotDto {
    private long id;
    private String projectName;
    private String projectDescription;
    private String projectType;
    private String regDt;
    private String modDt;
    private String modelName;
    private int completedStep;
    private String language;
    private String sampleRate;
    private String eos;
    private List<LearningData> learningDataList;
    private int epochSize;
    private int batchSize;
    private double rate;
    private String sampleLength;
    private int minNoiseSample;
    private int maxNoiseSample;
    private int minSnr;
    private int maxSnr;
    private List<NoiseData> noiseDataList;
    private PreTrainedModel preTrainedModel;

}
