package ai.mindslab.automl.backend.model.entity.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "deploy_snapshot")
public class DeploySnapshot {
    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String deployId;

    private Long projectSnapshotId;

    @Lob
    private String snapshot;

    private String endpoint;

    private String apiId;

    private String apiKey;

    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime regDt;
}
