package ai.mindslab.automl.backend.model.dto.deploy;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DeploySnapshotCreateDto {
    @ApiModelProperty(value = "프로젝트 스냅샷 ID")
    private Long projectSnapshotId;
    @ApiModelProperty(value = "배포 생성시 배포에게 지어주는 이름")
    private String modelId;
    @ApiModelProperty(value = "배포에 사용될 학습된 모델의 경로")
    private String amFilePath;
    @ApiModelProperty(value = "배포에 사용될 학습된 모델의 파일명")
    private String amFileName;
    @ApiModelProperty(value = "lm모델을 학습 모델에 맞추어 구성해주는 언어 파라미터")
    private String language;
    @ApiModelProperty(value = "lm모델을 학습 모델에 맞추어 구성해주는 샘플링레이트 파라미터")
    private String sampleRate;
    @ApiModelProperty(value = "lm모델을 학습 모델에 맞추어 구성해주는 eos 여부 파라미터")
    private String eos;
}
