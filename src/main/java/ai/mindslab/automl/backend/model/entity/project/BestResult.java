package ai.mindslab.automl.backend.model.entity.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "best_result")
public class BestResult {
    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long projectSnapshotId;

    private int tokenEpoch;

    @Column(precision = 5, scale = 2)
    private BigDecimal tokenError;

    private int wordEpoch;

    @Column(precision = 5, scale = 2)
    private BigDecimal wordError;

    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    private LocalDateTime regDt;

    @UpdateTimestamp
    @Column(nullable = false)
    private LocalDateTime modDt;
}
