package ai.mindslab.automl.backend.model.entity.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "model_test_snapshot")
public class ModelTestSnapshot {
    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String modelTestId;

    private String amFilePath;

    private String amFileName;

    private String dataFilePath;

    private String dataFileName;

    private String modelId;

    @Lob
    private String result;

    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    private LocalDateTime regDt;
}
