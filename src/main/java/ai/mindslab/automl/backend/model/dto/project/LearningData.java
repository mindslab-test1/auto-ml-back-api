package ai.mindslab.automl.backend.model.dto.project;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import java.time.LocalDateTime;

@Data
public class LearningData {

    private String atchFileId;
    private String atchFileDisp;
    private String atchFilePath;
    private String atchFileName;
    private String orgFileName;
    private String atchFileType;
    private String atchFileSize;
    private String smplRate;
    private Long playSec;
    private Long playSecAvg;
    private Long minPlaySec;
    private Long maxPlaySec;
    private int totFileCount;
    private String langCd;
    private String useEosYn;
    private String regDt;
    private String engnType;
}
