package ai.mindslab.automl.backend.model.dto.deploy;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DeploySnapshotReq {
    @ApiModelProperty(value = "Snapshot ID")
    private Long snapshotId;

    @ApiModelProperty(value = "모델 API 명")
    private String deployName;
}
