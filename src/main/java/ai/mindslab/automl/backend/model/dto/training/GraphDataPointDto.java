package ai.mindslab.automl.backend.model.dto.training;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import lombok.Data;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Data
public class GraphDataPointDto {
    private String graphType;
    private BigDecimal train;

    private Map<String, Object> devError;

    public void setDevError(String key, Object value){
        if (this.devError == null) {
            this.devError = new HashMap<>();
        }
        this.devError.put(key, value);
    }

    @JsonAnyGetter
    public Map<String, Object> getDevError() {
        return devError;
    }
}
