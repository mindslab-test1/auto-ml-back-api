package ai.mindslab.automl.backend.model.entity.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "model_in_training")
public class ModelInTraining {
    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long projectSnapshotId;

    private String trainingId;

    private int currEpoch;

    private int maxEpoch;

    @CreationTimestamp
    @Column(nullable = false,updatable = false)
    private LocalDateTime regDt;
}
