package ai.mindslab.automl.backend.model.dto.training;

import lombok.Data;

@Data
public class ModelTestSnapshotDto {
    private String modelTestId;
    private String amFilePath;
    private String amFileName;
    private String dataFilePath;
    private String dataFileName;
}
