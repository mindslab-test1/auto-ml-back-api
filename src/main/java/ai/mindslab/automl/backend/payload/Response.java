package ai.mindslab.automl.backend.payload;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Response {
    @ApiModelProperty(value = "응답 결과 여부")
    private Boolean success;
    @ApiModelProperty(value = "응답 결과 메세지")
    private String message;
}
