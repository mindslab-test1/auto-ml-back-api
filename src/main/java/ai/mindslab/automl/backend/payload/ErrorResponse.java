package ai.mindslab.automl.backend.payload;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ErrorResponse extends Response{
    @ApiModelProperty(value = "오류 상태 코드")
    private Integer errorCode;
    
    public ErrorResponse(boolean success, String message, int errorCode) {
        super(success,message);
        this.errorCode = errorCode;
    }
}
