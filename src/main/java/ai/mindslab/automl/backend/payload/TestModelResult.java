package ai.mindslab.automl.backend.payload;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class TestModelResult extends Response{
    @ApiModelProperty(value = "모델 테스트 응답 상태")
    private String status;
    @ApiModelProperty(value = "모델 테스트 응답 결과")
    private String result;
    @ApiModelProperty(value = "모델 테스트 Snapshot ID")
    private Long modelTestSnapshotId;
    @ApiModelProperty(value = "모델 테스트 대기열 순번")
    private Integer queueIndex;

    public TestModelResult(Boolean success, String message, String status, String result, Long modelTestSnapshotId, Integer queueIndex){
        super(success, message);
        this.status = status;
        this.result = result;
        this.modelTestSnapshotId = modelTestSnapshotId;
        this.queueIndex = queueIndex;
    }
}
