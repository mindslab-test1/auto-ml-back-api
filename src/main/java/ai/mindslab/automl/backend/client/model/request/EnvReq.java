package ai.mindslab.automl.backend.client.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class EnvReq implements Serializable {
    private String name;
    private String value = "";
}
