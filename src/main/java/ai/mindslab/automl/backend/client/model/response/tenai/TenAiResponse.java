package ai.mindslab.automl.backend.client.model.response.tenai;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TenAiResponse {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Map<String, Object> mssg = new HashMap<>();

    @JsonAnySetter
    public void setMssg(String key, Object value) {
        mssg.put(key, value);
    }

}
