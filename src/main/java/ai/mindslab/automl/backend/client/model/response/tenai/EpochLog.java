package ai.mindslab.automl.backend.client.model.response.tenai;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EpochLog {
    private String epoch;
    private String lr;
    private String lrcriterion;
    private String bch_ms;
    private String smp_ms;
    private String fwd_ms;
    private String crit;
    private String runtime;
    private String bwd_ms;
    private String optim_ms;
    private String loss;
    private String train_TER;
    private String train_WER;
    private Map<String, String> dev_errors = new HashMap<>();
    private String avg_isz;
    private String avg_tsz;
    private String max_tsz;
    private String hrs;
    private String thrpt;

    @JsonProperty("crit-fwd_ms")
    public void setCrit (String value) {
        this.crit = value;
    }

    @JsonProperty("train-TER")
    public void setTrain_TER (String value) {
        this.train_TER = value;
    }

    @JsonProperty("train-WER")
    public void setTrain_WER (String value) {
        this.train_WER = value;
    }

    @JsonProperty("avg-isz")
    public void setAvg_isz (String value) {
        this.avg_isz = value;
    }

    @JsonProperty("avg-tsz")
    public void setAvg_tsz (String value) {
        this.avg_tsz = value;
    }

    @JsonProperty("max-tsz")
    public void setMax_tsz (String value) {
        this.max_tsz = value;
    }

    @JsonProperty("thrpt_sec/sec")
    public void setThrpt (String value) {
        this.thrpt = value;
    }

    @JsonAnySetter
    public void setDevErrors(String key, String value) {
        dev_errors.put(key, value);
    }
}
