package ai.mindslab.automl.backend.client.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ModelForm {
    private String userId;
    private String mdlId;
    private String engnType;
    private String mdlFilePath;
    private String langCd;
    private String smplRate;
    private String useEosYn;
}
