package ai.mindslab.automl.backend.client.model.response.tenai;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StateMessage {
    private String type;
    private String reason;
    private String message;
    private String state_time;
}
