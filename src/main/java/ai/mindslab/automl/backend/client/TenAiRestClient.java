package ai.mindslab.automl.backend.client;

import ai.mindslab.automl.backend.client.model.request.DeployForm;
import ai.mindslab.automl.backend.client.model.request.TestForm;
import ai.mindslab.automl.backend.client.model.request.TrainForm;
import ai.mindslab.automl.backend.client.model.response.tenai.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Service
public class TenAiRestClient {

    @Value("${api.tenai.url}")
    private String apiUrl;

    @Value("${api.tenai.port}")
    private int apiPort;

    private String requestUri;

    private Logger LOG = LoggerFactory.getLogger(TenAiRestClient.class);

    private RestTemplate restTemplate;

    public TenAiRestClient(RestTemplate restTemplate,
                           @Value("${api.tenai.url}") String apiUrl,
                           @Value("${api.tenai.port}") int apiPort) {
        this.restTemplate = restTemplate;
        this.requestUri = apiUrl + ":" + apiPort;
    }

    /**
     * GET REQUESTS
     */

    public Resource[] getResources() throws RuntimeException {
        return restTemplate.getForObject(requestUri + "/resource/", Resource[].class);
    }

    public TrainedModel[] getTrainedModels() throws RuntimeException {
        return restTemplate.getForObject(requestUri + "/training/", TrainedModel[].class);
    }

    public TrainedModel getTrainedModel(String name) throws RuntimeException {
        return restTemplate.getForObject(requestUri + "/training/" + name, TrainedModel.class);
    }

    public TrainedModelFile[] getTrainedModelFiles(String name) throws RuntimeException {
        return restTemplate.getForObject(requestUri + "/training/modelfile/" + name, TrainedModelFile[].class);
    }

    public TrainedModelLog[] getTrainedModelLogs(String name, int epoch) throws RuntimeException {
        return restTemplate.getForObject(requestUri + "/training/logs/" + name + "/" + epoch, TrainedModelLog[].class);
    }

    public TestedModel getTestedModel(String modelTestName) throws RuntimeException {
        return restTemplate.getForObject(requestUri + "/test/" + modelTestName, TestedModel.class);
    }

    public TestedModel[] getTestedModels() throws RuntimeException {
        return restTemplate.getForObject(requestUri + "/test/", TestedModel[].class);
    }

    public DeployedModel getDeployedModel(String deployId) throws RuntimeException {
        return restTemplate.getForObject(requestUri + "/deploy/" + deployId, DeployedModel.class);
    }

    public DeployedModel[] getDeployedModels() throws RuntimeException {
        return restTemplate.getForObject(requestUri + "/deploy/", DeployedModel[].class);
    }

    /**
     * POST REQUESTS
     */

    public void addModelToTrain(TrainForm form) throws RuntimeException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<TrainForm> entity = new HttpEntity<>(form, headers);

        String result = restTemplate.postForObject(requestUri + "/training/", entity, String.class);
    }

    public void addModelToTest(TestForm form) throws RuntimeException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<TestForm> entity = new HttpEntity<>(form, headers);

        restTemplate.postForEntity(requestUri + "/test/", entity, null);
    }

    public void addModelToDeploy(DeployForm form) throws RuntimeException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<DeployForm> entity = new HttpEntity<>(form, headers);

        restTemplate.postForEntity(requestUri + "/deploy/", entity, null);
    }

    /**
     * PUT REQUESTS
     */
    public void endTraining(String name) throws RuntimeException {
        restTemplate.put(requestUri + "/training/" + name, null);
    }

    /**
     * DELETE REQUESTS
     */

    public void deleteTrainedModel(String name) throws RuntimeException {
        restTemplate.delete(requestUri + "/training/" + name);
    }

    public void deleteModelsInTesting(String name) throws RuntimeException {
        restTemplate.delete(requestUri + "/test/" + name);
    }

    public void deleteDeployeddModels(String name) throws RuntimeException {
        restTemplate.delete(requestUri + "/deploy/" + name);
    }
}
