package ai.mindslab.automl.backend.client.model.response.data;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ModelFilePostResponse extends DataRestResponse {
    private boolean data;
}

