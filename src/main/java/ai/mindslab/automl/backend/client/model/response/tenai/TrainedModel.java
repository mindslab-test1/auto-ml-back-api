package ai.mindslab.automl.backend.client.model.response.tenai;

import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TrainedModel implements Serializable {
    private String name;
    private String image;
    private StateMessage state;
    private String training_root_dir;
}