package ai.mindslab.automl.backend.client.model.response.data;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class DataRestResponse {
    private boolean success;
    private String error;
    private int targetRow;
    private String targetId;
    private int recordsTotal;
    private int recordsFiltered;
    private int rowCnt;
    private String failStack;
    private int pageBlock;
    private int totalPage;
    private int page;
    private String txDate;
}
