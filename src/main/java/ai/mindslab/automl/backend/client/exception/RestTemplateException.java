package ai.mindslab.automl.backend.client.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RestTemplateException extends RuntimeException{
    @Override
    public String getMessage() {
        return this.error;
    }

    private int statusCode;
    private String error;
}
