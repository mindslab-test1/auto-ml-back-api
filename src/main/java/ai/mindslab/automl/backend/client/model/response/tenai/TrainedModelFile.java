package ai.mindslab.automl.backend.client.model.response.tenai;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TrainedModelFile {
    private String name;
    private String path;
    private Long size;
}
