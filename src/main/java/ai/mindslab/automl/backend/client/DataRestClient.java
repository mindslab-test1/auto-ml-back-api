package ai.mindslab.automl.backend.client;

import ai.mindslab.automl.backend.client.model.request.ModelForm;
import ai.mindslab.automl.backend.client.model.response.data.DataRestResponse;
import ai.mindslab.automl.backend.client.model.response.data.ModelFilePostResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Service
public class DataRestClient {

    @Value("${api.data.url}")
    private String apiUrl;

    @Value("${api.data.port}")
    private int apiPort;

    private String requestUri;

    private Logger LOG = LoggerFactory.getLogger(TenAiRestClient.class);

    private RestTemplate restTemplate;

    public DataRestClient(RestTemplate restTemplate,
                          @Value("${api.data.url}") String apiUrl,
                          @Value("${api.data.port}") int apiPort) {
        this.restTemplate = restTemplate;
        this.requestUri = apiUrl + ":" + apiPort;
    }

    /**
     * POST REQUESTS
     */
    public void addModelData(ModelForm form) throws RuntimeException {
        URI uriWithParams = UriComponentsBuilder.fromUriString(requestUri + "/api/mmng/postModelFileInfo")
                .queryParam("userId", form.getUserId())
                .queryParam("mdlId", form.getMdlId())
                .queryParam("engnType", form.getEngnType())
                .queryParam("mdlFilePath", form.getMdlFilePath())
                .queryParam("langCd", form.getLangCd())
                .queryParam("smplRate", form.getSmplRate())
                .queryParam("useEosYn", form.getUseEosYn())
                .build().toUri();

        ModelFilePostResponse result = restTemplate.postForObject(uriWithParams, null, ModelFilePostResponse.class);
        LOG.info("Result: " + result.toString());
    }
}
