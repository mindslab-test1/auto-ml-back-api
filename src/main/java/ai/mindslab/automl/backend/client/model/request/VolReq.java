package ai.mindslab.automl.backend.client.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class VolReq implements Serializable {
    private String host_path;
    private String container_path;
}
