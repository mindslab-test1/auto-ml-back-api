package ai.mindslab.automl.backend.client.model.response.tenai;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeployedModel implements Serializable {
    private String name;
    private String image;
    private String deploy_host;
    private String deploy_port;
    private StateMessage state;
}
