package ai.mindslab.automl.backend.client.model.request;

import ai.mindslab.automl.backend.client.model.request.EnvReq;
import ai.mindslab.automl.backend.client.model.request.VolReq;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class TestForm implements Serializable {
    private String name;
    private String image;
    private List<String> command;
    private List<EnvReq> env;
    private List<VolReq> volumes;
}
