package ai.mindslab.automl.backend.client.model.response.tenai;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Lob;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TestedModel implements Serializable {
    private String name;
    private String image;
    private StateMessage state;
    private Integer queueIndex;
    @Lob
    private String result;
}
