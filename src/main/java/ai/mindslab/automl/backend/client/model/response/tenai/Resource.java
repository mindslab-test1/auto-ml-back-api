package ai.mindslab.automl.backend.client.model.response.tenai;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Resource implements Serializable {
    private String host_ip;
    private String gpu_smi_name;

    @JsonProperty(value="is_allocated")
    private boolean is_allocated;
    private String current_owner_kind;
    private String current_owner_name;
}
