package ai.mindslab.automl.backend.controller;

import ai.mindslab.automl.backend.client.model.response.tenai.StateMessage;
import ai.mindslab.automl.backend.model.dto.training.ProjectSnapshotReq;
import ai.mindslab.automl.backend.payload.ErrorResponse;
import ai.mindslab.automl.backend.payload.Response;
import ai.mindslab.automl.backend.service.QueueService;
import ai.mindslab.automl.backend.service.TrainingService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/training")
public class TrainingController {
    private static final Logger logger = LoggerFactory.getLogger(TrainingController.class.getName());

    private final TrainingService trainingService;
    private final QueueService queueService;

    /**
     * beginTraining
     * ProjectSnapshot에 저장된 학습 파라미터로 학습서버에 training entity를 생성해준다.
     * 학습에 사용 가능한 리소스가 있을시에만 학습 생성이 되며,
     * 리소스가 없을경우 대기열 (queue)에 projectSnapshotId로 저장된다.
     *
     * @param payload projectSnapshotId와 trainingId, 즉 화면설계서상 모델 아이디 "mdl_userID_엔진타입_생성일시"
     * @return ResponseEntity<ApiResponse> 학습 생성에 성공/실패 하였는지에 대한 적절한 응답 메세지
     */
    @Operation(
            summary = "학습 생성", description = "학습 생성 요청 => TEN AI",
            responses = {
                    @ApiResponse(
                            responseCode = "200", description = "학습 생성 요청 성공",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))
                    ),
                    @ApiResponse(
                            responseCode = "500", description = "학습 생성 요청 실패",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class)))
            }
    )
    @PostMapping("/begin")
    public ResponseEntity<Response> beginTraining(@RequestBody ProjectSnapshotReq payload) {
        logger.info("[Begin Training] Begin!");
        try {
            if (trainingService.checkAvailableResources() && !queueService.checkQueueExists()) {
                logger.info("[Begin Training] Adding model for training, snapshotId: {}", payload.getSnapshotId());
                trainingService.createTraining(payload.getSnapshotId());
                return new ResponseEntity<>(new Response(true, "Training started: " + payload.getTrainingId()), HttpStatus.OK);
            } else {
                logger.info("[Begin Training] Adding model to queue, snapshotId: {}", payload.getSnapshotId());
                queueService.addQueue(payload.getSnapshotId(), "training");
                return new ResponseEntity<>(new Response(true, "Training in queue: " + payload.getTrainingId()), HttpStatus.OK);
            }
        } catch (EntityNotFoundException e) {
            logger.error("[Begin Training] Error while starting training! {}", e.getMessage());
            return new ResponseEntity<>(new ErrorResponse(false, "Could not find project snapshot", HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            logger.error("[Begin Training] Error {}", e.getMessage());
            return new ResponseEntity<>(new ErrorResponse(false, "Internal Server Error", HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * finishTraining
     * 진행중인 학습에 대해 유저가 학습 종료를 요청했을시 학습 중단을 하기위한 기능 (화면설계서 페이지 21)
     * 현재 학습진행상황을 확인하여 status가 "Running"일 경우 학습 중단 요청.
     * 이미 완료 되었거나, 학습도중 오류가 났거나 학습이 이미 삭제되고 있는 중이면 그에대한 적절한 메세지 출력
     *
     * @param payload projectSnapshotId와 trainingId, 즉 화면설계서상 모델 아이디 "mdl_userID_엔진타입_생성일시"
     * @return ResponseEntity<ApiResponse> 학습 종료에 성공/실패 하였는지에 대한 적절한 응답 메세지
     */
    @Operation(
            summary = "학습 종료", description = "학습 종료 요청 => TEN AI",
            responses = {
                    @ApiResponse(
                            responseCode = "200", description = "학습 종료 요청 성공",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))
                    ),
                    @ApiResponse(
                            responseCode = "404", description = "학습 모델명/Project Snapshot NOT FOUND",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))
                    ),
                    @ApiResponse(
                            responseCode = "500", description = "학습 종료 요청 실패",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class)))
            }
    )
    @PutMapping("/finish")
    public ResponseEntity<Response> finishTraining(@RequestBody ProjectSnapshotReq payload) {
        logger.info("[Finish Training] Begin!");
        StateMessage state;

        try {
            logger.info("[Finish Training] Retrieving training status for TrainingId: {}", payload.getTrainingId());
            state = trainingService.getModelStatusByTrainingId(payload.getTrainingId());
        } catch (Exception e) {
            logger.error("[Finish Training] Error in finding training in ModelInTraining table, TrainingId: {}", payload.getTrainingId());
            return new ResponseEntity<>(new ErrorResponse(false, "Error. Failed to find model in training", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        }

        try {
            if (state.getType().equals("Completed")) {
                logger.info("[Finish Training] TrainingId: {} has already completed... No further action!", payload.getTrainingId());
                return new ResponseEntity<>(new Response(true, "Model training already completed"), HttpStatus.OK);
            } else if (state.getType().equals("Running")) {
                logger.info("[Finish Training] TrainingId: {} is running, requesting stop...", payload.getTrainingId());
                trainingService.endTraining(payload.getTrainingId(), payload.getSnapshotId());
                return new ResponseEntity<>(new Response(true, "Model  " + payload.getTrainingId() + " completed"), HttpStatus.OK);
            } else if (state.getType().equals("Error")) {
                logger.error("[Finish Training] TrainingId: {} had an error during training... No further action!", payload.getTrainingId());
                return new ResponseEntity<>(new Response(true, "Model  " + payload.getTrainingId() + " error"), HttpStatus.OK);
            } else if (state.getType().equals("Deleting")) {
                logger.info("[Finish Training] TrainingId: {} is being deleted... No further action!", payload.getTrainingId());
                return new ResponseEntity<>(new Response(true, "Model  " + payload.getTrainingId() + " deleting"), HttpStatus.OK);
            } else { // Waiting
                logger.info("[Finish Training] TrainingId: {} has not begun training... No further action!", payload.getTrainingId());
                return new ResponseEntity<>(new Response(true, "Model  " + payload.getTrainingId() + " waiting"), HttpStatus.OK);
            }
        } catch (EmptyResultDataAccessException e) {
            logger.error("[Finish Training] TrainingId: {} was not found in ModelInTraining table!", payload.getTrainingId());
            return new ResponseEntity<>(new ErrorResponse(false, "Error. Failed to find project snapshot", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        } catch (IndexOutOfBoundsException e) {
            logger.error("[Finish Training] TrainingId: {} trained model files not found!", payload.getTrainingId());
            return new ResponseEntity<>(new ErrorResponse(false, "Error. Failed to find trained model files.", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            logger.error("[Finish Training] TrainingId: {} an error occurred while attempting to stop!", payload.getTrainingId());
            return new ResponseEntity<>(new ErrorResponse(false, "Error. Failed to end training.", HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * cancelModelInQueue
     * 대기열에 있는 학습에 대해 유저가 학습 취소를 요청했을시 대기열에서 제거해주는 기능 (화면설계서 페이지 20)
     * 특정 projectSnapshot이 대기열에 존재하지 않는경우 에러메세지 출력
     *
     * @param payload projectSnapshotId와 trainingId, 즉 화면설계서상 모델 아이디 "mdl_userID_엔진타입_생성일시"
     * @return ResponseEntity<ApiResponse> 학습 취소에 성공/실패 하였는지에 대한 적절한 응답 메세지
     */
    @Operation(
            summary = "학습 대기 취소", description = "Queue 학습 모델 삭제 => DB",
            responses = {
                    @ApiResponse(
                            responseCode = "200", description = "학습 대기 취소 성공",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))
                    ),
                    @ApiResponse(
                            responseCode = "404", description = "학습 모델명  NOT FOUND",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))
                    ),
                    @ApiResponse(
                            responseCode = "500", description = "학습 대기 취소 실패",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class)))
            }
    )
    @PutMapping("/cancel")
    public ResponseEntity<Response> cancelModelInQueue(@RequestBody ProjectSnapshotReq payload) {
        logger.info("[Cancel Model In Queue] Begin!");
        try {
            logger.info("[Cancel Model In Queue] Deleting projectSnapshotId \"{}\" from queue...", payload.getSnapshotId());
            queueService.deleteQueue(payload.getSnapshotId());
            logger.info("[Cancel Model In Queue] ProjectSnapshotId \"{}\" successfully removed from queue!", payload.getSnapshotId());
            return new ResponseEntity<>(new Response(true, "Model  " + payload.getTrainingId() + " cancelled"), HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            logger.error("[Cancel Model In Queue] ProjectSnapshotId \"{}\" was not found in the queue...", payload.getSnapshotId());
            return new ResponseEntity<>(new ErrorResponse(false, "Error. Failed to find model in queue", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            logger.error("[Cancel Model In Queue] Error occurred while attempting to delete projectSnapshotId \"{}\" from queue...", payload.getSnapshotId());
            return new ResponseEntity<>(new ErrorResponse(false, "Error. Failed to cancel.", HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    @GetMapping("/test/{id}")
//    public void multiThreadTest (@PathVariable(name = "id") int testId) throws InterruptedException {
//        logger.info("Test #{} started!", testId);
//        logger.info("Test #{} sleeping...", testId);
//        Thread.sleep(5000);
//        logger.info("Test #{} awake!", testId);
//        logger.info("Test #{} complete!", testId);
//    }
}
