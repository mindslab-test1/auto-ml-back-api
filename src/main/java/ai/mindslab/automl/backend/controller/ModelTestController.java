package ai.mindslab.automl.backend.controller;

import ai.mindslab.automl.backend.client.exception.RestTemplateException;
import ai.mindslab.automl.backend.client.model.response.tenai.TestedModel;
import ai.mindslab.automl.backend.model.dto.test.ModelTestDto;
import ai.mindslab.automl.backend.model.entity.project.ModelTestSnapshot;
import ai.mindslab.automl.backend.payload.ErrorResponse;
import ai.mindslab.automl.backend.model.entity.project.Queue;
import ai.mindslab.automl.backend.payload.Response;
import ai.mindslab.automl.backend.payload.TestModelResult;
import ai.mindslab.automl.backend.service.ModelTestService;
import ai.mindslab.automl.backend.service.QueueService;
import ai.mindslab.automl.backend.service.util.FfmpegService;
import com.google.common.io.Files;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.dom4j.rule.Mode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/test")
public class ModelTestController {
    private static final Logger logger = LoggerFactory.getLogger(ModelTestController.class.getName());

    private final ModelTestService modelTestService;
    private final QueueService queueService;

    @Value("${mnt.path}")
    private String mntPath;

    @Value("${mnt.testData.path}")
    private String testDataPath;

    /**
     * beginModelTest
     * 모델 테스트 생성을 요청하는 기능. 요청문에 들어온 파라미터로 modelTestSnapshot 객체를 생성한 후,
     * 리소스가 있으면 테스트 서버에 테스트 생성 요청을, 리소스가 없으면 대기열에 보낸다.
     * @param modelId 테스트 요청을 보낸 학습 프로젝트의 아이디
     * @param modelFileName 테스트에 사용할 학습된 모델 파일 이름
     * @param modelFilePath 테스트에 사용할 학습된 모델 파일 경로
     * @param dataFile      테스트에 사용할 음성 데이터
     * @return ResponseEntity<Response> 적절한 응답 메세지 (테스트 시작 / 대기열 추가)
     */
    @Operation(
            summary = "모델 테스트 생성", description = "모델 테스트 생성 요청 => TEN AI",
            responses = {
                    @ApiResponse(
                            responseCode = "200", description = "모델 테스트 생성 요청 성공",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))
                    ),
                    @ApiResponse(
                            responseCode = "400", description = "모델 테스트 생성 요청 파라미터 오류",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))
                    ),
                    @ApiResponse(
                            responseCode = "500", description = "모델 테스트 생성 요청 실패",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class)))
            }
    )
    @PostMapping("/begin")
    public ResponseEntity<Response> beginModelTest(@RequestPart("modelId") String modelId,
                                                   @RequestPart("modelFileName") String modelFileName,
                                                   @RequestPart("modelFilePath") String modelFilePath,
                                                   @RequestPart("file") MultipartFile dataFile) {
        logger.info("[Begin Model Test] Begin!");
        try {
            logger.info("[Begin Model Test] Adding new model for testing!");
            String newFileName = modelTestService.addTimestampToFileName(Objects.requireNonNull(dataFile.getOriginalFilename()));
            if (!dataFile.isEmpty()) {
                File convertedFile = modelTestService.convertTestDataSampleRate(dataFile, modelId);


                File newFile = new File(mntPath + testDataPath + "/" + newFileName);
                Files.copy(convertedFile, newFile);
            } else {
                return new ResponseEntity<>(new ErrorResponse(false, "Model test encountered an error! Reason: empty dataFile", HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
            }

            ModelTestDto modelTestDto = ModelTestDto.builder()
                    .amFileName(modelFileName)
                    .amFilePath(modelFilePath)
                    .dataFileName(newFileName)
                    .dataFilePath(mntPath + testDataPath)
                    .modelId(modelId)
                    .build();
            ModelTestSnapshot snapshot = modelTestService.saveModelTestSnapshot(modelTestDto);

            logger.info("[Begin Model Test] ModelTestSnapshot successfully created! ModelTestSnapshotId: {}", snapshot.getId());
            return modelTestService.sendToResourceOrQueue(snapshot.getId());
        } catch (IllegalArgumentException e) {
            logger.error("[Begin Model Test] dataFile name is invalid. Error: {}", e.getMessage());
            return new ResponseEntity<>(new ErrorResponse(false, "dataFile name \"" + dataFile.getOriginalFilename() + "\" is invalid.", HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
        } catch (NullPointerException e) {
            logger.error("[Begin Model Test] Error while mounting audio file to NAS server! Error: {}", e.getMessage());
            return new ResponseEntity<>(new ErrorResponse(false, "Error while mounting audio file to NAS server!", HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (RestTemplateException e) {
            logger.error("[Begin Model Test] Error during model test create request to the training server! Error: {}", e.getMessage());
            return new ResponseEntity<>(new ErrorResponse(false, "Error during request to the training server!", HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            logger.error("[Begin Model Test] Error: {}", e.getMessage());
            return new ResponseEntity<>(new ErrorResponse(false, "Internal Server Error", HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * getModelTestStatus
     * 모델 테스트가 대기열에 있거나 테스트가 진행중이면 상태값을 조회하여 응답해준다.
     *
     * @param modelTestSnapshotId 모델 테스트와 관련된 modelTestSnapshot의 아이디
     * @return ResponseEntity<Response> 적절한 응답 상태 메세지
     */
    @Operation(
            summary = "모델 테스트 조회", description = "모델 테스트 조회 요청 => DB",
            responses = {
                    @ApiResponse(
                            responseCode = "200", description = "모델 테스트 조회 요청 성공",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = TestModelResult.class))
                    ),

                    @ApiResponse(
                            responseCode = "500", description = "모델 테스트 조회 요청 실패",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class)))
            }
    )
    @GetMapping("/status")
    public ResponseEntity<Response> getModelTestStatus(@RequestParam Long modelTestSnapshotId) {
        try {
            logger.info("[Get Model Test Status] Begin!");
            TestedModel testedModelResult = modelTestService.getTestedModelResult(modelTestSnapshotId);
            
            logger.info("[Get Model Test Status] Successfully retrieved the test status! ModelTestSnapshotId: {}", modelTestSnapshotId);
            
            return new ResponseEntity<>(new TestModelResult(true, "", testedModelResult.getState().getType(), testedModelResult.getResult(), modelTestSnapshotId, testedModelResult.getQueueIndex()), HttpStatus.OK);
            
        } catch (RestTemplateException e) {
            logger.error("[Get Model Test Status] Error during model test get request to the training server! ModelTestSnapshotId: {}!", modelTestSnapshotId);
            return new ResponseEntity<>(new ErrorResponse(false, "Requested model test was not found in testing server!", HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            logger.error("[Get Model Test Status] Error occurred while attempting to get model test status! ModelTestSnapshotId: {}!", modelTestSnapshotId);
            return new ResponseEntity<>(new ErrorResponse(false, "Internal Server Error", HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * cancelModelTest
     * 대기열에 있거나 실행중인 모델 테스트를 취소하는 기능.
     *
     * @param modelTestSnapshotId 모델 테스트와 관련된 modelTestSnapshot의 아이디
     * @return ResponseEntity<Response> 적절한 응답 상태 메세지
     */
    @Operation(
            summary = "모델 테스트 취소", description = "모델 테스트 취소 요청 => DB",
            responses = {
                    @ApiResponse(
                            responseCode = "200", description = "모델 테스트 취소 요청 성공",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))
                    ),
                    @ApiResponse(
                            responseCode = "200", description = "실행중인 모델 테스트 NOT FOUND",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))
                    ),
                    @ApiResponse(
                            responseCode = "500", description = "모델 테스트 취소 요청 실패",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class)))
            }
    )
    @PutMapping("/cancel")
    public ResponseEntity<Response> cancelModelTest (@RequestParam Long modelTestSnapshotId) {
        logger.info("[Cancel Model Test] Begin!");
        try {
            logger.info("[Cancel Model Test] Checking if modelTestSnapshotId \"{}\" is in queue...", modelTestSnapshotId);
            Queue queue = queueService.getQueueBySnapshotId(modelTestSnapshotId);
            if (queue != null) {
                logger.info("[Cancel Model Test] modelTestSnapshotId \"{}\" found in queue! Deleting from queue...", modelTestSnapshotId);
                queueService.deleteQueue(modelTestSnapshotId);
                logger.info("[Cancel Model Test] modelTestSnapshotId \"{}\" deleted from queue successfully!", modelTestSnapshotId);
                return new ResponseEntity<>(new Response(true, "Model test found in queue! Deleted successfully!"), HttpStatus.OK);
            }
            else {
                logger.info("[Cancel Model Test] Deleting modelTestSnapshotId \"{}\" from testing server...", modelTestSnapshotId);
                modelTestService.deleteModelsInTesting(modelTestSnapshotId);
                logger.info("[Cancel Model Test] ModelTestSnapshotId \"{}\" successfully deleted from testing server!", modelTestSnapshotId);
                return new ResponseEntity<>(new Response(true, "Model test found in training server! Deleted successfully!"), HttpStatus.OK);
            }
        } catch (RestTemplateException e) {
            logger.error("[Cancel Model Test] modelTestSnapshotId \"{}\" is not found from testing server!!", modelTestSnapshotId);
            return new ResponseEntity<>(new ErrorResponse(false, "Requested model test was not found in testing server!", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            logger.error("[Cancel Model Test] Error occurred while attempting to delete modelTestSnapshotId \"{}\" from testing server!!", modelTestSnapshotId);
            return new ResponseEntity<>(new ErrorResponse(false, "Internal Server Error", HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
