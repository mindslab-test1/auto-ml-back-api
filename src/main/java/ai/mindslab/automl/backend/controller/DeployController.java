package ai.mindslab.automl.backend.controller;

import ai.mindslab.automl.backend.model.dto.deploy.DeploySnapshotCreateDto;
import ai.mindslab.automl.backend.model.entity.project.DeploySnapshot;
import ai.mindslab.automl.backend.payload.ErrorResponse;
import ai.mindslab.automl.backend.payload.Response;
import ai.mindslab.automl.backend.service.DeployService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/deploy")
public class DeployController {
    private static final Logger logger = LoggerFactory.getLogger(DeployController.class.getName());

    private final DeployService deployService;

    /**
     * beginDeploy
     * API 배포를 요청하는 기능. 미리 준비된 DeploySnapshot의 snapshot 컬럼에서 배포 생성 파라미터를 가져온 후 배포 서버에 요청을 보낸다.
     *
     * @param payload DeploySnapshotId (DeploySnapshot 테이블에서의 ID) 와 DeployId (배포 서버에서 지어질 이름)
     * @return ResponseEntity<Response> 적절한 응답 상태 메세지
     */
    @Operation(
            summary = "배포 생성", description = "배포 생성 요청 => TEN AI",
            responses = {
                    @ApiResponse(
                            responseCode = "200", description = "배포 생성 요청 성공",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))
                    ),
                    @ApiResponse(
                            responseCode = "500", description = "배포 생성 요청 실패",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class)))
            }
    )
    @PostMapping("/add")
    public ResponseEntity<Response> beginDeploy(@RequestBody DeploySnapshotCreateDto payload) {
        logger.info("[Begin Deploy] Begin!");
        try {
            logger.info("[Begin Deploy] Adding model for training, snapshotId: {}", payload.getProjectSnapshotId());

            DeploySnapshot deploySnapshot = deployService.addDeploySnapshot(payload);
            deployService.createDeploy(deploySnapshot.getId());
            return new ResponseEntity<>(new Response(true, "API deployed! Model ID: " + payload.getModelId() + "; DeploySnapshotId: " + deploySnapshot.getId()), HttpStatus.OK);
        } catch (Exception e) {
            logger.error("[Begin Deploy] Error {}", e.getMessage());
            return new ResponseEntity<>(new ErrorResponse(false, "Internal Server Error", HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    /**
//     * addSnapshot
//     * DeploySnapshot을 만들어주는 기능. 하단 dto에 형식을 맞추어 POST 요청을 보내면 DB에 DeploySnapshot 객체가 생성됨.
//     *
//     * @param dto {
//     *            "projectSnapshotId": 75, // 프로젝트 스냅샷의 아이디
//     *            "modelId": "8_STT_20201111192111649", // 배포 생성시 배포에게 지어주는 이름
//     *            "amFilePath": "/AUTOML/training_result/training1/model", // 배포에 사용될 학습된 모델의 경로
//     *            "amFileName": "001_model_dev-num.bin", // 배포에 사용될 학습된 모델의 파일명
//     *            "language": "kor", // lm모델을 학습 모델에 맞추어 구성해주는 언어 파라미터
//     *            "sampleRate": "8000", // lm모델을 학습 모델에 맞추어 구성해주는 샘플링레이트 파라미터
//     *            "eos": "Y" // lm모델을 학습 모델에 맞추어 구성해주는 eos 여부 파라미터
//     *            }
//     * @return ResponseEntity<Response> 적절한 응답 상태 메세지
//     */
//    @PostMapping("/addSnapshot")
//    public ResponseEntity<Response> addSnapshot(@RequestBody DeploySnapshotCreateDto dto) {
//        logger.info("[Add Snapshot] Begin!");
//        try {
//            logger.info("[Add Snapshot] Creating a deploySnapshot entity in database...");
//            DeploySnapshot snapshot = deployService.addDeploySnapshot(dto);
//            logger.info("[Add Snapshot] Deploy Snapshot is created in database! DeploySnapshotId: {}", snapshot.getId());
//            return new ResponseEntity<>(new Response(true, "Deploy Snapshot Created: " + snapshot.getId()), HttpStatus.OK);
//        } catch (Exception e) {
//            logger.error("[Add Snapshot] Error in creating deploySnapshot! DeployId: {}", dto.getModelId());
//            return new ResponseEntity<>(new Response(false, "Deploy snapshot creation failed. DeploySnapshotId: " + dto.getModelId()), HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }

    /**
     * updateEndpoint
     * 생성된 배포가 리소스에 등록되어 실행되면, deploy_host와 deploy_port가 자동 생성됨. 이 둘을 합쳐 유저가 사용할 수 있는
     * API endpoint를 구성하여, DB에 저장해줌.
     *
     * @param deploySnapshotId 생성된 모델 배포와 연결된 deploySnapshot의 Id.
     * @return ResponseEntity<Response> 적절한 응답 상태 메세지
     */
    @Operation(
            summary = "Endpoint 업데이트", description = "Endpoint 업데이트 요청",
            responses = {
                    @ApiResponse(
                            responseCode = "200", description = "Endpoint 업데이트 요청 성공",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class))
                    ),
                    @ApiResponse(
                            responseCode = "500", description = "Endpoint 업데이트 요청 실패",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class)))
            }
    )
    @PutMapping("/endpoint/{id}")
    public ResponseEntity<Response> updateEndpoint(@PathVariable(name = "id") Long deploySnapshotId) {
        logger.info("[Update Endpoint] Begin!");
        try {
            logger.info("[Update Endpoint] Getting information on the deployed model, deploySnapshotId: {}", deploySnapshotId);
            String endpoint = deployService.getDeployEndpoint(deploySnapshotId);
            logger.info("[Update Endpoint] API endpoint for the deployed model updated successfully! DeploySnapshotId: {}, Endpoint: {}", deploySnapshotId, endpoint);
            return new ResponseEntity<>(new Response(true, "API Endpoint Update Successful! DeploySnapshotId: " + deploySnapshotId + ", Endpoint: " + endpoint), HttpStatus.OK);
        } catch (Exception e) {
            logger.error("[Update Endpoint] API endpoint update was unsuccessful. DeploySnapshotId: " + deploySnapshotId);
            return new ResponseEntity<>(new ErrorResponse(false, "API Endpoint Update Failed! DeploySnapshotId: " + deploySnapshotId, HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
