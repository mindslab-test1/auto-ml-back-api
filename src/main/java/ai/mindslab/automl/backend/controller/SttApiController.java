package ai.mindslab.automl.backend.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

@Slf4j
@RestController
@RequiredArgsConstructor
@ApiIgnore
@RequestMapping("/api/stt")
public class SttApiController {
    @PostMapping("/")
    String getSttResult(@RequestPart("ID") String id, @RequestPart("key") String key, @RequestPart("lang") String lang,
                        @RequestPart("sampling") int sampling, @RequestPart("model") String model, @RequestPart("file") MultipartFile file){
        // API KEY/ID Check

        // If authenticated
        // Send POST request

        return null;
    }
}
